package net.lugdunon.world.defaults.character;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

import net.lugdunon.command.CommandProperties;
import net.lugdunon.command.ex.CommandNotSupportedException;
import net.lugdunon.command.ex.IsNotServerInvokedCommand;
import net.lugdunon.io.EnhancedDataOutputStream;
import net.lugdunon.math.Point;
import net.lugdunon.server.worldgen.WorldGenerator;
import net.lugdunon.state.State;
import net.lugdunon.state.character.Character;
import net.lugdunon.state.character.CharacterItemUse;
import net.lugdunon.state.character.PlayerCharacter;
import net.lugdunon.state.character.modifier.BaseModifier;
import net.lugdunon.state.character.stats.ICharacterResourceChangeDelegate;
import net.lugdunon.state.character.stats.ICharacterStats;
import net.lugdunon.state.item.Item;
import net.lugdunon.state.item.ItemInstance;
import net.lugdunon.state.item.PlaceableItemInstance;
import net.lugdunon.state.item.RechargingInstance;
import net.lugdunon.state.item.action.BaseChargedAction;
import net.lugdunon.state.metric.IMetricListener;
import net.lugdunon.state.metric.spell.ModifierWithActor;
import net.lugdunon.util.FastMath;
import net.lugdunon.world.politics.Faction;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DefaultCharacterStats implements ICharacterStats, IMetricListener
{
	public static final long   DEFAULT_REGEN_INTERVAL     =6000; // SIX SECONDS
	public static       long   REGEN_INTERVAL             =-1;
	
	public static       long   DEFAULT_ADV_COST_CEILING   =20;   // 2^20
	public static       long   ADV_COST_CEILING           =-1;
	
	public static final int    STAT_MODIFIER_MODE_ADD     =0;
	public static final int    STAT_MODIFIER_MODE_SUBTRACT=1;

	public static final String STAT_KEY_CRAFT_SPEC        ="CRAFT_";

	public static final String STAT_KEY_MOVEMENT_SPEED_INCREASE="movementSpeedIncrease";
	
	public static final String STAT_KEY_HEALTH_INCREASE        ="healthIncrease";
	public static final String STAT_KEY_STAMINA_INCREASE       ="staminaIncrease";
	public static final String STAT_KEY_HEALTH_REGEN_INCREASE  ="healthRegenIncrease";
	public static final String STAT_KEY_STAMINA_REGEN_INCREASE ="staminaRegenIncrease";

	public static final String STAT_KEY_HEALED_BONUS           ="healedBonus";
	public static final String STAT_KEY_HEALING_BONUS          ="healingBonus";
	public static final String STAT_KEY_FOOD_BONUS             ="foodBonus";
	public static final String STAT_KEY_FEED_BONUS             ="feedBonus";
	public static final String STAT_KEY_HARVEST_BONUS          ="damageBonus";
	public static final String STAT_KEY_DAMAGE_BONUS           ="damageBonus";
	public static final String STAT_KEY_DEFENSE_BONUS          ="defenseBonus";

	public static final String STAT_KEY_HARVEST_BONUS_SPEC     =STAT_KEY_HARVEST_BONUS+"_";
	public static final String STAT_KEY_DAMAGE_BONUS_SPEC      =STAT_KEY_DAMAGE_BONUS +"_";
	public static final String STAT_KEY_DEFENSE_BONUS_SPEC     =STAT_KEY_DEFENSE_BONUS+"_";
	public static final String STAT_KEY_SPELL_SCHOOL_BONUS_SPEC="spellSchoolBonus_";
	
	private Logger                                    logger;
	private ICharacterResourceChangeDelegate          resourceChangeDelegate;
	private Character                                 character;
	private ConcurrentLinkedQueue<String>             advancements;
	private ConcurrentLinkedQueue<BaseModifier>       modifiers;
	private ConcurrentLinkedQueue<RechargingInstance> itemsRecharging;
	        Map<String, Object>                       statModifiers;
	private double[]                                  vulnerabilityMatrix;
	private int                                       currentHealth;
	private int                                       currentStamina;
	private long                                      currentExperience;
	private long                                      lifetimeExperience;
	
	private int                                       baseHealth;
	private int                                       baseStamina;

	private int                                       regenAccumulatedDelta;
	private int                                       baseHealthRegenRate;
	private int                                       baseStaminaRegenRate;
	
	private BaseChargedAction                         itemChargingAction;
	private long[]                                    itemCharging;
	
	private Map<String,Long>                          achievements;  

	private Map<Long,Integer>                         factions;
	
	public DefaultCharacterStats()
	{
		if(!WorldGenerator.isNoState())
		{
			if(REGEN_INTERVAL == -1)
			{
				REGEN_INTERVAL=State.instance().getWorld().getWorldConfigProperty("default.character.stats.regen.interval", DefaultCharacterStats.DEFAULT_REGEN_INTERVAL);
			}
	
			if(ADV_COST_CEILING == -1)
			{
				ADV_COST_CEILING=State.instance().getWorld().getWorldConfigProperty("default.character.stats.advancement.cost.ceiling", DefaultCharacterStats.DEFAULT_ADV_COST_CEILING);
			}
		}
		
		logger               =LoggerFactory.getLogger(DefaultCharacterStats.class);
		advancements         =new ConcurrentLinkedQueue<String>();
		statModifiers        =new ConcurrentHashMap<String,Object>();
		itemsRecharging      =new ConcurrentLinkedQueue<RechargingInstance>();
		modifiers            =new ConcurrentLinkedQueue<BaseModifier      >();
		vulnerabilityMatrix  =new double[]{1.00,1.00,1.00,1.50,1.00,1.50,2.00,2.00,2.00};
		currentHealth        = -1;
		currentStamina       = -1;
		currentExperience    =  0;
		lifetimeExperience   =  0;
		
		baseHealth           =100;
		baseStamina          =100;
		
		regenAccumulatedDelta=0;
		baseHealthRegenRate  =0;
		baseStaminaRegenRate =0;
		
		itemChargingAction   =null;
		itemCharging         =new long[]{0,0};
		
		achievements         =new ConcurrentHashMap<String,Long   >();
		factions             =new ConcurrentHashMap<Long  ,Integer>();
	}

	@Override
	public void init()
	{
		this.baseHealthRegenRate   =character.getBaseHealthRegenRate ();
		this.baseStaminaRegenRate  =character.getBaseStaminaRegenRate();
		this.baseHealth            =character.getBaseHealth          ();
		this.baseStamina           =character.getBaseStamina         ();
		this.resourceChangeDelegate=character;
		
		Set<String> ceks=new TreeSet<String>();
		
		for(ItemInstance eq:character.getInventoryBlock(Character.EQUIPMENT_BLOCK))
		{
			try
			{
				if(eq != null && eq.getItemDef().getProps().has("effects"))
				{
					JSONObject effects=eq.getItemDef().getProps().getJSONObject("effects");
					String[] eks      =JSONObject.getNames(effects);
					
					for(String ek:eks)
					{
						ceks.add(ek);
						updateStatModifier(STAT_MODIFIER_MODE_ADD,ek,effects.get(ek));
					}
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		
		if(currentHealth == -1 || currentHealth > getMaximumHealth())
		{
			currentHealth=getMaximumHealth();
		}
		
		if(currentStamina == -1 || currentStamina > getMaximumStamina())
		{
			currentStamina=getMaximumStamina();
		}

		String[] effectKeys=ceks.toArray(new String[ceks.size()]);
		Arrays.sort(effectKeys);
		
		if(resourceChangeDelegate != null)
		{
			informResourceChangeDelegate(null,effectKeys);
		}
		
		//CHECK FOR AND ASSIGN ABSENT EARNED ACHIEVEMENTS
		if(this.character instanceof PlayerCharacter)
		{
			State.instance().getWorld().getAchievementDefinitions().checkForMissingAchievements((PlayerCharacter) this.character);
		}
	}

	@Override
    public void initMetrics()
    {
		if(this.character instanceof PlayerCharacter)
		{
			((PlayerCharacter) this.character).getMetricsManager().addListener("CORE.METRIC.ACTION.DRINK.CONSUMED",this);
			((PlayerCharacter) this.character).getMetricsManager().addListener("CORE.METRIC.ACTION.FOOD.CONSUMED", this);
			
			State.instance().getWorld().getAchievementDefinitions().registerAchievements((PlayerCharacter) this.character,this);
		}
    }

	@Override
    public void metricUpdated(String metricType, Object metricValue, Object transientValue)
    {
		try
		{
			if("CORE.METRIC.ACTION.DRINK.CONSUMED".equals(metricType))
			{
				//EVERY 10th
				if(((JSONObject) metricValue).getInt("total")%10 == 0)
				{
					applyModifier("DEBUFF.INTOXICATED",null);
				}
			}
			else if("CORE.METRIC.ACTION.FOOD.CONSUMED".equals(metricType))
			{
				//EVERY 10th
				if(((JSONObject) metricValue).getInt("total")%10 == 0)
				{
					applyModifier("DEBUFF.SATED",null);
				}
			}

			//metricUpdated() IS ONLY CALLED FOR PLAYER CHARACTERS
			State.instance().getWorld().getAchievementDefinitions().checkAchievements(metricType,metricValue,transientValue,(PlayerCharacter) this.character);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
    }
	
	@Override
	public void update(long delta)
	{
		//REGEN
		if(!character.isDead() && (DefaultCharacterStats.REGEN_INTERVAL > 0) && ((regenAccumulatedDelta+=delta) >= DefaultCharacterStats.REGEN_INTERVAL))
		{
			regenAccumulatedDelta=0;
			
			if((getHealthRegenRate() > 0) && (currentHealth < getMaximumHealth()))
			{
				modifyHealth(getHealthRegenRate(),ICharacterStats.HEALTH_MODIFY_CAUSE_REGEN,null);
			}
			
			if((getStaminaRegenRate() > 0) && (currentStamina < getMaximumStamina()))
			{
				modifyStamina(getStaminaRegenRate(),ICharacterStats.STAMINA_MODIFY_CAUSE_REGEN,null);
			}
		}
		
		//UPDATE RECHARGE ON ITEMS
		List<RechargingInstance> ritm=new ArrayList<RechargingInstance>();
		
		for(RechargingInstance ri:itemsRecharging)
		{
			ri.updateAccumulatedDelta(delta);
			
			if(ri.isRecharged())
			{
				ritm.add(ri);
			}
		}
		
		itemsRecharging.removeAll(ritm);
		
		//ITEM IS CHARGING, SO UPDATE CHARGE
		if(itemChargingAction != null)
		{
			itemCharging[0]+=delta;
			
			if(itemCharging[0] > itemCharging[1])
			{
				BaseChargedAction ca=itemChargingAction;
				itemChargingAction=null;
				ca.actionFullyCharged();
			}
		}
		
		//UPDATE MODIFIERS
		List<BaseModifier> mtm=new ArrayList<BaseModifier>();
		
		for(BaseModifier m:modifiers)
		{
			m.update(delta);
			
			if(m.expired())
			{
				mtm.add(m);
			}
		}
		
		modifiers.removeAll(mtm);
	}
	
	
	////
	
	private double getSchoolBonus(Character applier, String school)
	{
		double amount=0.0;
		
		if(
			((DefaultCharacterStats) applier.getCharacterStats()).statModifiers.containsKey(DefaultCharacterStats.STAT_KEY_SPELL_SCHOOL_BONUS_SPEC+school)
		)
		{
			amount+=(Double) ((DefaultCharacterStats) applier.getCharacterStats()).statModifiers.get(DefaultCharacterStats.STAT_KEY_SPELL_SCHOOL_BONUS_SPEC+school);
		}
		
		return(amount);
	}
	

	@Override
	public boolean applyModifier(String modifierId, Character applier)
	{
		BaseModifier bm=null;
		boolean      ap=false;
		
		if(hasModifier(modifierId))
		{
			for(BaseModifier m:modifiers)
			{
				if(m.getId().equals(modifierId))
				{
					bm=m;

					if(applier != null)
					{
						ap=bm.reapply(getSchoolBonus(applier,bm.getSchool()),applier);
					}
					else
					{
						ap=bm.reapply(0.0,null);
					}
					
					break;
				}
			}
		}
		
		if(bm == null)
		{
			try
	        {
	            bm=State.instance().getWorld().getModifierDefinitions().newModifier(this,modifierId);

				if(applier != null)
				{
					bm.setValueModifierWithApplier(getSchoolBonus(applier,bm.getSchool()),applier);
				}
				else
				{
					bm.setValueModifierWithApplier(0.0,null);
				}
	        }
	        catch (JSONException e)
	        {
	        	logger.error("Error applying modifier {}.",modifierId,e);
	            //e.printStackTrace();
	        }
			
			if(bm != null)
			{
				ap=modifiers.add(bm);
			}
		}
		
		if(bm != null && ap)
		{
			resourceChangeDelegate.handleModifierApplyEvent(modifierId, applier);

			if(getCharacter() instanceof PlayerCharacter)
			{
				((PlayerCharacter) getCharacter()).getMetricsManager().handleMetricUpdate(
					"CORE.METRIC.SPELL.MODIFIER.GAINED",
					(applier != null)?new ModifierWithActor(modifierId, applier):modifierId
				);
			}
			
			if((applier != null) && applier instanceof PlayerCharacter)
			{
				((PlayerCharacter) applier).getMetricsManager().handleMetricUpdate(
					"CORE.METRIC.SPELL.MODIFIER.APPLIED",
					new ModifierWithActor(modifierId,getCharacter())
				);
			}
			
			try
			{
				CommandProperties props=new CommandProperties();

				props.setModifier("modifier",bm  );
				props.setBoolean ("complete",true);
				
			    State.instance().getGame().addIncomingRequest(
					"CORE.COMMAND.SPELL.MODIFIER.APPLY",
					props
			    );
			}
            catch (CommandNotSupportedException e)
            {
	            e.printStackTrace();
            }
            catch (IsNotServerInvokedCommand e)
            {
	            e.printStackTrace();
            }
			
			return(true);
		}
		
		return(false);
	}

	@Override
	public void expireModifier(String modifierId, Character dispeller)
	{
		if(hasModifier(modifierId))
		{
			BaseModifier bm=null;
			
			for(BaseModifier m:modifiers)
			{
				if(m.getId().equals(modifierId))
				{
					bm=m;
					break;
				}
			}
			
			if((bm != null) && (dispeller == null || !bm.isDispellable()))
			{
				if(getCharacter() instanceof PlayerCharacter)
				{
					((PlayerCharacter) getCharacter()).getMetricsManager().handleMetricUpdate(
						"CORE.METRIC.SPELL.MODIFIER.EXPIRED",
						(dispeller != null)?new ModifierWithActor(modifierId, dispeller):modifierId
					);
				}
				
				if((dispeller != null) && dispeller instanceof PlayerCharacter)
				{
					((PlayerCharacter) dispeller).getMetricsManager().handleMetricUpdate(
						"CORE.METRIC.SPELL.MODIFIER.DISPELLED",
						new ModifierWithActor(modifierId,getCharacter())
					);
				}
				
				bm.expire();
			}
		}
	}

	@Override
	public boolean hasModifier(String modifierId)
	{
		for(BaseModifier bm:modifiers)
		{
			if(bm.getId().equals(modifierId))
			{
				return(true);
			}
		}
		
		return(false);
	}

	@Override
	public final Collection<BaseModifier> listModifiers()
	{
		return(modifiers);
	}
	
	
	////
	
	@Override
	public long getAchievementAwarded(String achievementId)
	{
		if(achievements.containsKey(achievementId))
		{
			return(achievements.get(achievementId));
		}
		
		return(ICharacterStats.ACHIEVEMENT_UNAWARDED);
	}
	

	@Override
	public void awardAchievement (String achievementId)
	{
		achievements.put(achievementId,System.currentTimeMillis());
	
	    try
	    {
		    CommandProperties props=new CommandProperties();
		   
		    props.setCharacter("character",  character);
		    props.setString   ("achievement",achievementId);
	
			State.instance().getGame().addIncomingRequest(
				"CORE.COMMAND.PLAYER.ACHIEVEMENT",
				props
			);
	    }
	    catch(CommandNotSupportedException e)
	    {
	    	e.printStackTrace();
	    }
	    catch(IsNotServerInvokedCommand e)
	    {
	    	e.printStackTrace();
	    }
	}
	
	@Override
	public final Iterable<String> listAwardedAchievements()
	{
		return(achievements.keySet());
	}
	
	////
	

	@Override
    public void awardExperience(long experience)
    {
	    currentExperience +=experience;
	    lifetimeExperience+=experience;
	    
	    try
	    {
		    CommandProperties props=new CommandProperties();
		   
		    props.setCharacter("character",character);
		    props.setInt      ("mode",     ICharacterStats.EXPERIENCE_GAINED);
	
			State.instance().getGame().addIncomingRequest(
				"CORE.COMMAND.PLAYER.EXPERIENCE",
				props
			);
	    }
	    catch(CommandNotSupportedException e)
	    {
	    	e.printStackTrace();
	    }
	    catch(IsNotServerInvokedCommand e)
	    {
	    	e.printStackTrace();
	    }
    }

	@Override
    public boolean deductExperience(long experience)
    {
		if(currentExperience < experience)
		{
			return(false);
		}
		
	    currentExperience-=experience;
	    
	    try
	    {
		    CommandProperties props=new CommandProperties();
		   
		    props.setCharacter("character",character);
		    props.setInt      ("mode",     ICharacterStats.EXPERIENCE_GAINED);
	
			State.instance().getGame().addIncomingRequest(
				"CORE.COMMAND.PLAYER.EXPERIENCE",
				props
			);
	    }
	    catch(CommandNotSupportedException e)
	    {
	    	e.printStackTrace();
	    }
	    catch(IsNotServerInvokedCommand e)
	    {
	    	e.printStackTrace();
	    }
	    
		return(true);
    }

	@Override
    public long getCurrentExperience()
    {
	    return currentExperience;
    }

	@Override
    public long getLifetimeExperience()
    {
	    return lifetimeExperience;
    }

	@Override
    public long getAdvancementCost(String id)
    {
	    return((long) Math.pow(2,advancements.size()>ADV_COST_CEILING?ADV_COST_CEILING:advancements.size()));
    }
	
	@Override
	public void setCharacter(Character character)
	{
		this.character=character;
	}
	
	@Override
	public Character getCharacter()
	{
		return(character);
	}
	
	////
	

	public boolean advancementSatisfiesPrerequisites(String id)
	{
		try
		{
			if(State.instance().getWorld().getAdvancements().getAdvancement(id).has("prerequisite"))
			{
				String prerequisite=State.instance().getWorld().getAdvancements().getAdvancement(id).getString("prerequisite");
				
				if(advancements.contains(prerequisite))
				{
					return(advancementSatisfiesPrerequisites(prerequisite));
				}
				else
				{
					return(false);
				}
			}
			
			return(true);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return(false);
		}
	}
	
	private boolean isPrerequisite(String id)
	{
		try
		{
			for(String adv:advancements)
			{
				if(State.instance().getWorld().getAdvancements().getAdvancement(adv).has("prerequisite"))
				{
					String prerequisite=State.instance().getWorld().getAdvancements().getAdvancement(adv).getString("prerequisite");
					
					if(id.equals(prerequisite))
					{
						return(true);
					}
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return(false);
	}

    @Override
	public boolean addAdvancement(String id)
	{
		if(advancements.contains(id) || !advancementSatisfiesPrerequisites(id))
		{
			return(false);
		}
		
		try
		{
			JSONObject advEffects=State.instance().getWorld().getAdvancements().getAdvancement(id).getJSONObject("effects");
			String[]   effectKeys=JSONObject.getNames(advEffects);

			Arrays.sort(effectKeys);
			
			for(String effectKey:effectKeys)
			{
				updateStatModifier(STAT_MODIFIER_MODE_ADD,effectKey,advEffects.get(effectKey));
			}
			
			advancements.add(id);
			
			if(resourceChangeDelegate != null)
			{
				informResourceChangeDelegate(id,effectKeys);
				
				resourceChangeDelegate.handleAdvancementChangeEvent(ICharacterStats.ADVANCEMENT_GAINED,id);
			}
			
			return(true);
		}
		catch(Exception e)
		{
			logger.error("Effect {} is invalid.",id);
			e.printStackTrace();
			
			return(false);
		}
	}

	public void informResourceChangeDelegate(String id, String[] effectKeys)
    {
		if(resourceChangeDelegate == null)
		{
			return;
		}
		
	    if(Arrays.binarySearch(effectKeys,DefaultCharacterStats.STAT_KEY_HEALTH_INCREASE) > -1)
	    {
	    	resourceChangeDelegate.handleHealthChangeEvent(ICharacterStats.MAXIMUM_HEALTH_RECALCULATED,ICharacterStats.HEALTH_MODIFY_CAUSE_ADVANCEMENT,id,-1,-1);
	    }
	    else if(Arrays.binarySearch(effectKeys,DefaultCharacterStats.STAT_KEY_STAMINA_INCREASE) > -1)
	    {
	    	resourceChangeDelegate.handleStaminaChangeEvent(ICharacterStats.MAXIMUM_STAMINA_RECALCULATED,ICharacterStats.STAMINA_MODIFY_CAUSE_ADVANCEMENT,id,-1,-1);
	    }
    }

	@Override
	public void removeAdvancement(String id)
	{
		if(!advancements.contains(id) || isPrerequisite(id))
		{
			return;
		}
		
		try
		{
			JSONObject advEffects=State.instance().getWorld().getAdvancements().getAdvancement(id).getJSONObject("effects");
			String[]   effectKeys=JSONObject.getNames(advEffects);

			Arrays.sort(effectKeys);
			
			for(String effectKey:effectKeys)
			{
				updateStatModifier(STAT_MODIFIER_MODE_SUBTRACT,effectKey,advEffects.get(effectKey));
			}

			advancements.remove(id);
			
			informResourceChangeDelegate(id,effectKeys);

			resourceChangeDelegate.handleAdvancementChangeEvent(ICharacterStats.ADVANCEMENT_LOST,id);
		}
		catch(Exception e)
		{
			logger.error("Effect {} is invalid.",id);
			e.printStackTrace();
		}
	}

	@Override
	public Iterator<String> listAdvancements()
	{
		return advancements.iterator();
	}
	
	////

	@Override
    public void setPostDeathStamina(int stamina)
    {
		try
		{
			JSONObject cause=new JSONObject();
			
			cause.put("value",  "postDeath"  );
			cause.put("display","Restoration");
			cause.put("icon",   "F236"       );
			
			modifyStamina(
				stamina,
				ICharacterStats.STAMINA_MODIFY_CAUSE_ENVIRONMENT,
				cause
			);
		}
		catch(Exception e)
		{
			;
		}
    }

	@Override
	public void setPostDeathHealth(final int health)
	{
		try
		{
			JSONObject cause=new JSONObject();

			cause.put("value",  "postDeath"  );
			cause.put("display","Restoration");
			cause.put("icon",   "F236"       );
			
			modifyHealth(
				health,
				ICharacterStats.HEALTH_MODIFY_CAUSE_ENVIRONMENT,
				cause
			);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public int getCurrentHealth()
	{
		return(currentHealth);
	}

	@Override
	public int getCurrentStamina()
	{
		return(currentStamina);
	}

	@Override
	public double getMovementSpeed()
	{
		if(statModifiers.containsKey(DefaultCharacterStats.STAT_KEY_MOVEMENT_SPEED_INCREASE))
		{
			return(1.0/(1.0+((Double) statModifiers.get(DefaultCharacterStats.STAT_KEY_MOVEMENT_SPEED_INCREASE))));
		}
		
		return(1.0);
	}

	@Override
	public int getMaximumHealth()
	{
		if(statModifiers.containsKey(DefaultCharacterStats.STAT_KEY_HEALTH_INCREASE))
		{
			return((int) Math.floor(baseHealth+(baseHealth*((Double) statModifiers.get(DefaultCharacterStats.STAT_KEY_HEALTH_INCREASE)))));
		}
		
		return(baseHealth);
	}

	@Override
	public int getMaximumStamina()
	{
		if(statModifiers.containsKey(DefaultCharacterStats.STAT_KEY_STAMINA_INCREASE))
		{
			return((int) Math.floor(baseStamina+(baseStamina*((Double) statModifiers.get(DefaultCharacterStats.STAT_KEY_STAMINA_INCREASE)))));
		}
		
		return(baseStamina);
	}
	
	@Override
    public int getHealthRegenRate()
    {
		if(statModifiers.containsKey(DefaultCharacterStats.STAT_KEY_HEALTH_REGEN_INCREASE))
		{
			return((int)(baseHealthRegenRate+((Long) statModifiers.get(DefaultCharacterStats.STAT_KEY_HEALTH_REGEN_INCREASE))));
		}
		
		return(baseHealthRegenRate);
    }

	@Override
    public int getStaminaRegenRate()
    {
		if(statModifiers.containsKey(DefaultCharacterStats.STAT_KEY_STAMINA_REGEN_INCREASE))
		{
			return((int)(baseStaminaRegenRate+((Long) statModifiers.get(DefaultCharacterStats.STAT_KEY_STAMINA_REGEN_INCREASE))));
		}
		
		return(baseStaminaRegenRate);
    }
	
	@Override
	public int getLootAmount(int dropAmount)
	{
		if(statModifiers.containsKey(DefaultCharacterStats.STAT_KEY_HARVEST_BONUS))
		{
			dropAmount+=(int) Math.round(((Double) statModifiers.get(DefaultCharacterStats.STAT_KEY_HARVEST_BONUS))*dropAmount);
		}
		
		return(dropAmount);
	}
	
	@Override
	public int getLootAmount(Item item, int dropAmount)
	{
		if(statModifiers.containsKey(DefaultCharacterStats.STAT_KEY_HARVEST_BONUS_SPEC+item.getItemId()))
		{
			dropAmount+=(int) Math.round(((Double) statModifiers.get(DefaultCharacterStats.STAT_KEY_HARVEST_BONUS_SPEC+item.getItemId()))*dropAmount)+getLootAmount(dropAmount);
		}
		
		return(dropAmount);
	}

	@Override
	public int getHealingAmount(int amount, int causeType, Object cause)
	{
		double modifier=0.0;
		
		if((causeType == ICharacterStats.HEALTH_MODIFY_CAUSE_ITEM_USE) && "FOOD".equals((((CharacterItemUse) cause).getItem()).getItemType()))
		{
			//TARGET'S FOOD BONUS
			if(statModifiers.containsKey(DefaultCharacterStats.STAT_KEY_FOOD_BONUS))
			{
				modifier+=(Double) statModifiers.get(DefaultCharacterStats.STAT_KEY_FOOD_BONUS);
			}

			//FEEDER'S FEED BONUS
			if(((DefaultCharacterStats) (((CharacterItemUse) cause).getCharacter()).getCharacterStats()).statModifiers.containsKey(DefaultCharacterStats.STAT_KEY_FEED_BONUS))
			{
				modifier+=(Double) ((DefaultCharacterStats) (((CharacterItemUse) cause).getCharacter()).getCharacterStats()).statModifiers.get(DefaultCharacterStats.STAT_KEY_FEED_BONUS);
			}
		}
		else if((causeType == ICharacterStats.HEALTH_MODIFY_CAUSE_ITEM_USE) && "SPELL".equals((((CharacterItemUse) cause).getItem()).getItemType()))
		{
			//TARGET'S HEALED BONUS
			if(statModifiers.containsKey(DefaultCharacterStats.STAT_KEY_HEALED_BONUS))
			{
				modifier+=(Double) statModifiers.get(DefaultCharacterStats.STAT_KEY_HEALED_BONUS);
			}
			
			//CASTER'S HEALING BONUS
			if(((DefaultCharacterStats) (((CharacterItemUse) cause).getCharacter()).getCharacterStats()).statModifiers.containsKey(DefaultCharacterStats.STAT_KEY_HEALING_BONUS))
			{
				modifier+=(Double) ((DefaultCharacterStats) (((CharacterItemUse) cause).getCharacter()).getCharacterStats()).statModifiers.get(DefaultCharacterStats.STAT_KEY_HEALING_BONUS);
			}
			
			//CACLULATE SPELL SCHOOL BONUS
			if(
				((((CharacterItemUse) cause).getItem()).getItemSpellSchool()) != null &&
				((DefaultCharacterStats) (((CharacterItemUse) cause).getCharacter()).getCharacterStats()).statModifiers.containsKey(DefaultCharacterStats.STAT_KEY_SPELL_SCHOOL_BONUS_SPEC+((CharacterItemUse) cause).getItem().getItemSpellSchool())
			)
			{
				modifier+=(Double) ((DefaultCharacterStats) (((CharacterItemUse) cause).getCharacter()).getCharacterStats()).statModifiers.get(DefaultCharacterStats.STAT_KEY_SPELL_SCHOOL_BONUS_SPEC+((CharacterItemUse) cause).getItem().getItemSpellSchool());
			}
		}
		else if(causeType == ICharacterStats.HEALTH_MODIFY_CAUSE_MODIFIER)
		{
			modifier+=((BaseModifier) cause).getValueModifier();
		}
//System.out.println(amount+" * "+modifier+" = "+(amount+Math.round(modifier*amount)));		
		amount+=(int) Math.round(modifier*amount);

		return(amount);
	}

	@Override
	public int getRestorativeAmount(int amount, int causeType, Object cause)
	{
		double modifier=0.0;
		
		if((causeType == ICharacterStats.STAMINA_MODIFY_CAUSE_ITEM_USE) && "FOOD".equals((((CharacterItemUse) cause).getItem()).getItemType()))
		{
			//TARGET'S FOOD BONUS
			if(statModifiers.containsKey(DefaultCharacterStats.STAT_KEY_FOOD_BONUS))
			{
				modifier+=(Double) statModifiers.get(DefaultCharacterStats.STAT_KEY_FOOD_BONUS);
			}

			//FEEDER'S FEED BONUS
			if(((DefaultCharacterStats) (((CharacterItemUse) cause).getCharacter()).getCharacterStats()).statModifiers.containsKey(DefaultCharacterStats.STAT_KEY_FEED_BONUS))
			{
				modifier+=(Double) ((DefaultCharacterStats) (((CharacterItemUse) cause).getCharacter()).getCharacterStats()).statModifiers.get(DefaultCharacterStats.STAT_KEY_FEED_BONUS);
			}
		}
		else if((causeType == ICharacterStats.STAMINA_MODIFY_CAUSE_ITEM_USE) && "SPELL".equals((((CharacterItemUse) cause).getItem()).getItemType()))
		{
			//TARGET'S HEALED BONUS
			if(statModifiers.containsKey(DefaultCharacterStats.STAT_KEY_HEALED_BONUS))
			{
				modifier+=(Double) statModifiers.get(DefaultCharacterStats.STAT_KEY_HEALED_BONUS);
			}
			
			//CASTER'S HEALING BONUS
			if(((DefaultCharacterStats) (((CharacterItemUse) cause).getCharacter()).getCharacterStats()).statModifiers.containsKey(DefaultCharacterStats.STAT_KEY_HEALING_BONUS))
			{
				modifier+=(Double) statModifiers.get(DefaultCharacterStats.STAT_KEY_HEALING_BONUS);
			}
			
			//CACLULATE SPELL SCHOOL BONUS
			if(
				((((CharacterItemUse) cause).getItem()).getItemSpellSchool()) != null &&
				((DefaultCharacterStats) (((CharacterItemUse) cause).getCharacter()).getCharacterStats()).statModifiers.containsKey(DefaultCharacterStats.STAT_KEY_SPELL_SCHOOL_BONUS_SPEC+((CharacterItemUse) cause).getItem().getItemSpellSchool())
			)
			{
				modifier+=(Double) ((DefaultCharacterStats) (((CharacterItemUse) cause).getCharacter()).getCharacterStats()).statModifiers.get(DefaultCharacterStats.STAT_KEY_SPELL_SCHOOL_BONUS_SPEC+((CharacterItemUse) cause).getItem().getItemSpellSchool());
			}
		}
		else if(causeType == ICharacterStats.STAMINA_MODIFY_CAUSE_MODIFIER)
		{
			modifier+=(int) Math.round(((BaseModifier) cause).getValueModifier()*amount);
		}
		
		amount+=(int) Math.round(modifier*amount);
	
		return(amount);
	}
	
	@Override
    public boolean satisfiesStaminaRequirement(Item item, int useType, boolean deduct)
    {
		int stamina   =0;
		int curStamina=getCurrentStamina();
		int causeType =ICharacterStats.STAMINA_MODIFY_CAUSE_TOOL_USE;
		
	    switch(useType)
	    {
	    	case Item.ITEM_USE_TOOL:
	    	{
	    		stamina  =item.getToolStamina  ();
	    		causeType=ICharacterStats.STAMINA_MODIFY_CAUSE_TOOL_USE;
	    		break;
	    	}
	    	case Item.ITEM_USE_WEAPON:
	    	{
	    		stamina  =item.getWeaponStamina();
	    		causeType=ICharacterStats.STAMINA_MODIFY_CAUSE_WEAPON_USE;
	    		break;
	    	}
	    }
	    
//	    if(item.getItemSpellSchool() != null)
//	    {
//	    	stamina-=Math.floor((stamina*getSchoolBonus(this.getCharacter(),item.getItemSpellSchool()))/2.0);
//	    }
	    
	    if(deduct)
	    {
	    	if((stamina != 0) && (stamina <= curStamina))
	    	{
			    if(item.isSpell())
			    {
			    	causeType=ICharacterStats.STAMINA_MODIFY_CAUSE_SPELL_USE;
			    }
			    
		    	modifyStamina(-stamina, causeType, item);
	    	}
	    	else
	    	{
	    		//SEND ERROR NOTIFICATION
	    		if(character instanceof PlayerCharacter)
	    		{
	    			((PlayerCharacter) character).sendStaminaLackingForActionMessage();
	    		}
	    	}
	    }
	    
	    return((stamina != 0) && (stamina <= curStamina));
    }
	
	@Override
    public void modifyStamina(int amount, int causeType, Object cause)
    {
		if(amount != 0)
		{
			currentStamina+=amount;
			
			if(currentStamina <= 0)
			{
				int uf=currentStamina;
				
				currentStamina=0;
				resourceChangeDelegate.handleStaminaChangeEvent(amount>0?ICharacterStats.STAMINA_GAINED:STAMINA_LOST,causeType,cause,amount,uf);
			}
			else if(currentStamina > getMaximumStamina())
			{
				int of=currentStamina-getMaximumStamina();
				
				currentStamina=getMaximumStamina();
				resourceChangeDelegate.handleStaminaChangeEvent(amount>0?ICharacterStats.STAMINA_GAINED:STAMINA_LOST,causeType,cause,amount,of);
			}
			else
			{
				resourceChangeDelegate.handleStaminaChangeEvent(amount>0?ICharacterStats.STAMINA_GAINED:STAMINA_LOST,causeType,cause,amount, 0);
			}
		}
    }

	@Override
	public void modifyHealth(int amount, int causeType, Object cause)
	{
	
		if(amount != 0)
		{
			currentHealth+=amount;
			
			if(currentHealth <= 0)
			{
				int uf=currentHealth;
					
				currentHealth=0;
				resourceChangeDelegate.handleHealthChangeEvent(ICharacterStats.CHARACTER_DEATH_OCCURRED,causeType,cause,amount,uf);
			}
			else if(currentHealth > getMaximumHealth())
			{
				int of=currentHealth-getMaximumHealth();
				
				currentHealth=getMaximumHealth();
				resourceChangeDelegate.handleHealthChangeEvent(amount>0?ICharacterStats.HEALTH_GAINED:HEALTH_LOST,causeType,cause,amount,of);
			}
			else
			{
				resourceChangeDelegate.handleHealthChangeEvent(amount>0?ICharacterStats.HEALTH_GAINED:HEALTH_LOST,causeType,cause,amount,0 );
			}
		}
	}

	@Override
	public void equipmentChanged(int slot, ItemInstance oldItem, ItemInstance newItem)
	{
		JSONArray  modifiers;
		JSONObject effects;
		String[]   eks;
		
		if(oldItem != null)
		{
			if(oldItem.getItemDef().getProps().has("effects"))
			{
				try
				{
					effects=oldItem.getItemDef().getProps().getJSONObject("effects");
					eks    =JSONObject.getNames(effects);
					
					for(String ek:eks)
					{
						updateStatModifier(STAT_MODIFIER_MODE_SUBTRACT,ek,effects.get(ek));
					}
				}
				catch(JSONException e)
				{
					e.printStackTrace();
				}
			}
			
			if(oldItem.getItemDef().getProps().has("modifiers"))
			{
				try
				{
					modifiers=oldItem.getItemDef().getProps().getJSONArray("modifiers");
					
					for(int i=0;i<modifiers.length();i++)
					{
						expireModifier(modifiers.getString(i), null);
					}
				}
				catch(JSONException e)
				{
					e.printStackTrace();
				}
			}
		}
		
		if(newItem != null)
		{
			if(newItem.getItemDef().getProps().has("effects"))
			{
				try
				{
					effects=newItem.getItemDef().getProps().getJSONObject("effects");
					eks    =JSONObject.getNames(effects);
					
					for(String ek:eks)
					{
						updateStatModifier(STAT_MODIFIER_MODE_ADD,ek,effects.get(ek));
					}
				}
				catch(JSONException e)
				{
					e.printStackTrace();
				}
			}
			
			if(newItem.getItemDef().getProps().has("modifiers"))
			{
				try
				{
					modifiers=newItem.getItemDef().getProps().getJSONArray("modifiers");
					
					for(int i=0;i<modifiers.length();i++)
					{
						applyModifier(modifiers.getString(i), null);
					}
				}
				catch(JSONException e)
				{
					e.printStackTrace();
				}
			}
		}
	}

	private void damageEquipment(Character damaged)
	{
		List<Integer> slots=new ArrayList<Integer>();
		
		for(int i=0;i<Character.EQUIPMENT_SIZE;i++)
		{
			if(
				damaged.getInventoryItem(i, Character.EQUIPMENT_BLOCK) != null &&
				damaged.getInventoryItem(i, Character.EQUIPMENT_BLOCK).isDurable()
			)
			{
				slots.add(i);
			}
		}
		
		if(slots.size() > 0)
		{
			try
			{
				CommandProperties props=new CommandProperties();
				
				props.setCharacter("character",damaged                                                                 );
				props.setInt      ("modify",   -1                                                                      );
				props.setInt      ("slotIndex",slots.get(State.instance().getWorld().getRandom().nextInt(slots.size())));
				props.setInt      ("slotType", Character.EQUIPMENT_BLOCK                                               );
				
	            State.instance().getGame().addIncomingRequest(
	        		"CORE.COMMAND.ITEM.DURABILITY.UPDATE",
	        		props
	            );
	         
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
	}

	@Override
	public boolean attacked(Character attacker, Item weapon)
	{
		int    dmg=(int) Math.round(getDamage(attacker,weapon)*getMitigationPercent(attacker,weapon));
		double em =getEffectivenessModifierForAttacker(attacker,character,weapon);
		double vm ;
		
		em =getEffectivenessModifierForAttacker(attacker,character,weapon);
		vm =getVulnerabilityModifierForDefender(attacker,character,weapon);
		dmg=(int) Math.round(-dmg*vm*em);
		
		//DAMAGE ATTACKER
		if(dmg > 0)
		{
			((DefaultCharacterStats) attacker.getCharacterStats()).modifyHealth(-dmg,ICharacterStats.HEALTH_MODIFY_CAUSE_ITEM_USE,new CharacterItemUse(attacker, weapon));
			damageEquipment(attacker);
		}
		else if(dmg < 0)
		{
			itemChargeInterrupted();
			
			modifyHealth(dmg,ICharacterStats.HEALTH_MODIFY_CAUSE_ITEM_USE,new CharacterItemUse(attacker, weapon));
			damageEquipment(getCharacter());
		}
	
		return(dmg < 0);
	}

	@Override
	public int getDamage(Character attacker, Item weapon)
	{
		try
		{
			JSONObject damageDef=null;
			
			if(weapon.getWeaponDef().get("damage") instanceof String)
			{
				damageDef=new JSONObject();
				
				if("$attackerCurrentHealth".equals(weapon.getWeaponDef().getString("damage")))
				{
					damageDef.put("exact", attacker.getCharacterStats().getCurrentHealth());
				}
				else if("$attackerMaximumHealth".equals(weapon.getWeaponDef().getString("damage")))
				{
					damageDef.put("exact", attacker.getCharacterStats().getMaximumHealth());
				}
				else if("$attackerBaseHealth".equals(weapon.getWeaponDef().getString("damage")))
				{
					damageDef.put("exact", attacker.getBaseHealth());
				}
				else if("$defenderCurrentHealth".equals(weapon.getWeaponDef().getString("damage")))
				{
					damageDef.put("exact", getCurrentHealth());
				}
				else if("$defenderMaximumHealth".equals(weapon.getWeaponDef().getString("damage")))
				{
					damageDef.put("exact", getMaximumHealth());
				}
				else if("$defenderBaseHealth".equals(weapon.getWeaponDef().getString("damage")))
				{
					damageDef.put("exact", getCharacter().getBaseHealth());
				}
				else
				{
					damageDef.put("exact", 0);
				}
			}
			else
			{
				damageDef=weapon.getWeaponDef().getJSONObject("damage");
			}
			
			int    damage=FastMath.valueFromRandomDefinition(damageDef);
			double bonus =0.0;
	
			if(((DefaultCharacterStats) attacker.getCharacterStats()).statModifiers.containsKey(DefaultCharacterStats.STAT_KEY_DAMAGE_BONUS))
			{
				bonus+=(Double) ((DefaultCharacterStats) attacker.getCharacterStats()).statModifiers.get(DefaultCharacterStats.STAT_KEY_DAMAGE_BONUS);
			}
			
			//CALCULATE WEAPON SPECIFIC
			if(((DefaultCharacterStats) attacker.getCharacterStats()).statModifiers.containsKey(DefaultCharacterStats.STAT_KEY_DAMAGE_BONUS_SPEC+weapon.getItemType()))
			{
				bonus+=(Double) ((DefaultCharacterStats) attacker.getCharacterStats()).statModifiers.get(DefaultCharacterStats.STAT_KEY_DAMAGE_BONUS_SPEC+weapon.getItemType());
			}
			
			//CACLULATE SPELL SCHOOL BONUS
			if(
				(weapon.getItemSpellSchool()) != null &&
				((DefaultCharacterStats) attacker.getCharacterStats()).statModifiers.containsKey(DefaultCharacterStats.STAT_KEY_SPELL_SCHOOL_BONUS_SPEC+weapon.getItemSpellSchool())
			)
			{
				bonus+=(Double) ((DefaultCharacterStats) attacker.getCharacterStats()).statModifiers.get(DefaultCharacterStats.STAT_KEY_SPELL_SCHOOL_BONUS_SPEC+weapon.getItemSpellSchool());
			}
			
			damage+=(int) Math.ceil(damage*bonus);
			
			return(damage);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return 0;
	}

	@Override
    public int getDamage(Character attacker, String school, int damage)
    {
		try
		{
			double bonus=0.0;
	
			if(((DefaultCharacterStats) attacker.getCharacterStats()).statModifiers.containsKey(DefaultCharacterStats.STAT_KEY_DAMAGE_BONUS))
			{
				bonus+=(Double) ((DefaultCharacterStats) attacker.getCharacterStats()).statModifiers.get(DefaultCharacterStats.STAT_KEY_DAMAGE_BONUS);
			}
			
			//CACLULATE SPELL SCHOOL BONUS
			if(
				school != null &&
				((DefaultCharacterStats) attacker.getCharacterStats()).statModifiers.containsKey(DefaultCharacterStats.STAT_KEY_SPELL_SCHOOL_BONUS_SPEC+school)
			)
			{
				bonus+=(Double) ((DefaultCharacterStats) attacker.getCharacterStats()).statModifiers.get(DefaultCharacterStats.STAT_KEY_SPELL_SCHOOL_BONUS_SPEC+school);
			}
			
			damage+=(int) Math.ceil(damage*bonus);
			
			return(damage);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return 0;
    }

	@Override
	public int getDamage(PlaceableItemInstance pii, Item tool)
	{
		try
		{
			int    damage=(int) Math.ceil(FastMath.valueFromRandomDefinition(tool.getToolDef().getJSONObject("damage"))*pii.getSusceptible().get(tool.getItemType()).getDouble("mod"));
			double bonus =0.0;
			
			if(statModifiers.containsKey(DefaultCharacterStats.STAT_KEY_DAMAGE_BONUS))
			{
				bonus+=(Double) statModifiers.get(DefaultCharacterStats.STAT_KEY_DAMAGE_BONUS);
			}
			
			//CALCULATE TOOL SPECIFIC
			if(statModifiers.containsKey(DefaultCharacterStats.STAT_KEY_DAMAGE_BONUS_SPEC+tool.getItemType()))
			{
				bonus+=(Double) statModifiers.get(DefaultCharacterStats.STAT_KEY_DAMAGE_BONUS_SPEC+tool.getItemType());
			}
			
			//CACLULATE SPELL SCHOOL BONUS
			if(
				(tool.getItemSpellSchool()) != null &&
				statModifiers.containsKey(DefaultCharacterStats.STAT_KEY_SPELL_SCHOOL_BONUS_SPEC+tool.getItemSpellSchool())
			)
			{
				bonus+=(Double) statModifiers.get(DefaultCharacterStats.STAT_KEY_SPELL_SCHOOL_BONUS_SPEC+tool.getItemSpellSchool());
			}
			
			damage+=(int) Math.ceil(damage*bonus);
	
			return(damage);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return 0;
	}

	@Override
	public double getMitigationPercent(Character attacker, Item weapon)
	{
		double mitigation=1.0;
		
		if(statModifiers.containsKey(DefaultCharacterStats.STAT_KEY_DEFENSE_BONUS))
		{
			mitigation-=((Double) statModifiers.get(DefaultCharacterStats.STAT_KEY_DEFENSE_BONUS));
		}
		
		if(statModifiers.containsKey(DefaultCharacterStats.STAT_KEY_DEFENSE_BONUS_SPEC+weapon.getItemType()))
		{
			mitigation-=((Double) statModifiers.get(DefaultCharacterStats.STAT_KEY_DEFENSE_BONUS_SPEC+weapon.getItemType()));
		}
		
		return(mitigation);
	}

	@Override
	public int getCraftingLevel(String craftType)
	{
		if(statModifiers.containsKey(STAT_KEY_CRAFT_SPEC+craftType))
		{
			return(((Long) statModifiers.get(STAT_KEY_CRAFT_SPEC+craftType)).intValue());
		}
		
		return(0);
	}
	
	@Override
	public long getWeaponCharge(Item weapon)
	{
		return(weapon.getWeaponCharge());
	}

	@Override
	public long getToolCharge(Item tool)
	{
		return(tool.getToolCharge());
	}
	
	@Override
	public long getWeaponRecharge(Item weapon)
	{
		return(weapon.getWeaponRecharge());
	}

	@Override
	public long getToolRecharge(Item tool)
	{
		return(tool.getToolRecharge());
	}
	
	@Override
	public boolean isItemChargeInProgress()
	{
		return(this.itemChargingAction != null);
	}
	
	@Override
	public BaseChargedAction getItemChargeInProgress()
	{
		return(this.itemChargingAction);
	}
	
	@Override
	public boolean itemChargeInterrupted()
	{
		if(this.itemChargingAction != null)
		{
			try
	        {
				CommandProperties props=new CommandProperties();

				props.setCharacter("character", this.character);
				props.setString   ("itemDefId", this.itemChargingAction.getItem().getItemId());
				props.setString   ("actionType",this.itemChargingAction.getItemUseType());
				props.setInt      ("mode",      1);
				
			    State.instance().getGame().addIncomingRequest(
		    		"CORE.COMMAND.CHARGE.ACTION",
		    		props
			    );
	        }
	        catch (CommandNotSupportedException e)
	        {
	            e.printStackTrace();
	        }
	        catch (IsNotServerInvokedCommand e)
	        {
	            e.printStackTrace();
	        }
			
			BaseChargedAction ca=itemChargingAction;
			itemChargingAction=null;
			ca.actionInterrupted();

			return(true);
		}
		
		return(false);
	}

	@Override
	public void itemChargeInitiated(BaseChargedAction ca)
	{
		if(this.itemChargingAction != null)
		{
			if(this.itemChargingAction.getItem().getItemId().equals(ca.getItem().getItemId()))
			{
				return;
			}
			
			itemChargeInterrupted();
		}

		this.itemChargingAction=ca;
		this.itemCharging[0]   =0;
		this.itemCharging[1]   =this.itemChargingAction.getCharge();
		
		try
        {
			CommandProperties props=new CommandProperties();

			props.setCharacter("character", this.character);
			props.setString   ("itemDefId", this.itemChargingAction.getItem().getItemId());
			props.setString   ("actionType",this.itemChargingAction.getItemUseType());
			props.setInt      ("mode",      0);
			
		    State.instance().getGame().addIncomingRequest(
	    		"CORE.COMMAND.CHARGE.ACTION",
	    		props
		    );
        }
        catch (CommandNotSupportedException e)
        {
            e.printStackTrace();
        }
        catch (IsNotServerInvokedCommand e)
        {
            e.printStackTrace();
        }
	}

	@Override
	public boolean itemUsedAsWeapon(Item item)
	{
		if(this.itemChargingAction != null)
		{
			if(!item.getItemId().equals(this.itemChargingAction.getItem().getItemId()))
			{
				itemChargeInterrupted();
			}
			
			return(false);
		}
		
		if(item.getWeaponCharge() == 0)
		{
			return(invokeRecharge(item,getWeaponRecharge(item)));
		}
		
		return(true);
	}

	@Override
	public boolean itemUsedAsTool(Item item)
	{
		if(this.itemChargingAction != null)
		{
			if(!item.getItemId().equals(this.itemChargingAction.getItem().getItemId()))
			{
				itemChargeInterrupted();
			}
			
			return(false);
		}

		if(item.getToolCharge() == 0)
		{
			return(invokeRecharge(item,getToolRecharge(item)));
		}
		
		return(true);
	}

	@Override
	public boolean invokeRecharge(Item item, long recharge)
	{
		RechargingInstance ri=new RechargingInstance(item,recharge);
		
		if(recharge > 0 && !this.itemsRecharging.contains(ri))
		{
			this.itemsRecharging.add(ri);
			return(true);
		}

		return(false);
	}

	@Override
	public boolean isRecharging(Item item)
	{
		for(RechargingInstance ri:itemsRecharging)
		{
			if(ri.matches(item))
			{
				return(true);
			}
		}
		
		return(false);
	}
	
	////

	@Override
	public boolean hasEncounteredFaction(long factionId)
	{
		return(factions.containsKey(factionId));
	}

	@Override
	public int alterFactionStanding(long factionId, int amount)
	{
		if(!hasEncounteredFaction(factionId))
		{
			Faction f=State.instance().getWorld().getPolitics().getFaction(factionId);
			
			if(f != null)
			{
				factions.put(factionId,f.getInitialStanding());
			}
		}
		
		factions.put(factionId, factions.get(factionId)+amount);
		
		//ALERT CLIENTS
		try
        {
			CommandProperties props=new CommandProperties();

			props.setCharacter("character",        this.character         );
			props.setLong     ("factionId",        factionId              );
			props.setInt      ("factionAlteration",amount                 );
			props.setInt      ("factionStanding",  factions.get(factionId));
			
		    State.instance().getGame().addIncomingRequest(
	    		"CORE.COMMAND.CHARACTER.STAT.ALTER.FACTION.STANDING",
	    		props
		    );
        }
        catch (CommandNotSupportedException e)
        {
            e.printStackTrace();
        }
        catch (IsNotServerInvokedCommand e)
        {
            e.printStackTrace();
        }
		
		return(factions.get(factionId));
	}

	@Override
	public int getFactionStanding(long factionId)
	{
		if(hasEncounteredFaction(factionId))
		{
			return(factions.get(factionId));
		}
		else
		{
			Faction f=State.instance().getWorld().getPolitics().getFaction(factionId);
			
			if(f != null)
			{
				factions.put(factionId,f.getInitialStanding());
				
				return(f.getInitialStanding());
			}
		}
		
		return(0);
	}

	@Override
	public int getFactionLevel(long factionId)
	{
		int fs=getFactionStanding(factionId);

		if(fs > 8250)
		{
			return(ICharacterStats.FACTION_LEVEL_EXALTED);
		}
		else if(fs <= 8250 && fs > 3250)
		{
			return(ICharacterStats.FACTION_LEVEL_REVERED);
		}
		else if(fs <= 3250 && fs > 1250)
		{
			return(ICharacterStats.FACTION_LEVEL_HONORED);
		}
		else if(fs <= 1250 && fs > 250)
		{
			return(ICharacterStats.FACTION_LEVEL_FRIENDLY);
		}
		else if(fs <= 250 && fs > -250)
		{
			return(ICharacterStats.FACTION_LEVEL_NEUTRAL);
		}
		else if(fs <= -250 && fs > -1250)
		{
			return(ICharacterStats.FACTION_LEVEL_UNFRIENDLY);
		}
		else if(fs <= -1250 && fs > -3250)
		{
			return(ICharacterStats.FACTION_LEVEL_SUSPECT);
		}
		else if(fs <= -3250 && fs > -8250)
		{
			return(ICharacterStats.FACTION_LEVEL_HOSTILE);
		}
		else //if(fs <= -8250)
		{
			return(ICharacterStats.FACTION_LEVEL_HATED);
		}
	}

	@Override
    public double getBuyModifierForFactionLevel(int factionLevel)
    {
		switch(factionLevel)
		{
			case ICharacterStats.FACTION_LEVEL_EXALTED:
			{
				return(0.50);
			}
			case ICharacterStats.FACTION_LEVEL_REVERED:
			{
				return(0.75);
			}
			case ICharacterStats.FACTION_LEVEL_HONORED:
			{
				return(0.80);
			}
			case ICharacterStats.FACTION_LEVEL_FRIENDLY:
			{
				return(0.95);
			}
			case ICharacterStats.FACTION_LEVEL_NEUTRAL:
			{
				return(1.00);
			}
			case ICharacterStats.FACTION_LEVEL_UNFRIENDLY:
			{
				return(1.50);
			}
			case ICharacterStats.FACTION_LEVEL_SUSPECT:
			{
				return(1.75);
			}
			case ICharacterStats.FACTION_LEVEL_HOSTILE:
			{
				return(2.00);
			}
			default:
			case ICharacterStats.FACTION_LEVEL_HATED:
			{
				return(2.00);
			}
		}
    }
	
	public int getStartingFactionStandingForFactionLevel(int factionLevel)
	{
		switch(factionLevel)
		{
			case ICharacterStats.FACTION_LEVEL_EXALTED:
			{
				return(8251);
			}
			case ICharacterStats.FACTION_LEVEL_REVERED:
			{
				return(3251);
			}
			case ICharacterStats.FACTION_LEVEL_HONORED:
			{
				return(1251);
			}
			case ICharacterStats.FACTION_LEVEL_FRIENDLY:
			{
				return(251);
			}
			case ICharacterStats.FACTION_LEVEL_NEUTRAL:
			{
				return(-250);
			}
			case ICharacterStats.FACTION_LEVEL_UNFRIENDLY:
			{
				return(-1250);
			}
			case ICharacterStats.FACTION_LEVEL_SUSPECT:
			{
				return(-3250);
			}
			case ICharacterStats.FACTION_LEVEL_HOSTILE:
			{
				return(-8250);
			}
			default:
			case ICharacterStats.FACTION_LEVEL_HATED:
			{
				return(-16250);
			}
		}
	}
	
	////

	@Override
	public void fromJSONObject(JSONObject o) throws JSONException
	{
		if(o.has("currentHealth"))
		{
			this.currentHealth=o.getInt("currentHealth");
		}
		else
		{
			this.currentHealth=1;
		}
		
		if(o.has("currentStamina"))
		{
			this.currentStamina=o.getInt("currentStamina");
		}
		else
		{
			this.currentStamina=1;
		}
		
		if(o.has("currentExperience"))
		{
			this.currentExperience=o.getLong("currentExperience");
		}
		else
		{
			this.currentExperience=1;
		}
		
		if(o.has("lifetimeExperience"))
		{
			this.lifetimeExperience=o.getLong("lifetimeExperience");
		}
		else
		{
			this.lifetimeExperience=1;
		}

		this.advancements   .clear();
		this.itemsRecharging.clear();
		this.modifiers      .clear();
		
		for(int i=0;i<this.vulnerabilityMatrix.length;i++)
		{
			this.vulnerabilityMatrix[i]=o.getJSONArray("vmatrix").getDouble(i);
		}
		
		for(int i=0;i<o.getJSONArray("advancements").length();i++)
		{
			addAdvancement(o.getJSONArray("advancements").getString(i));
		}
		
		if(o.has("itemsRecharging"))
		{
			for(int i=0;i<o.getJSONArray("itemsRecharging").length();i++)
			{
				this.itemsRecharging.add(
					new RechargingInstance(o.getJSONArray("itemsRecharging").getJSONObject(i))
				);
			}
		}
		
		if(o.has("modifiers"))
		{
			for(int i=0;i<o.getJSONArray("modifiers").length();i++)
			{
				BaseModifier bm=State.instance().getWorld().getModifierDefinitions().applyModifier(this,o.getJSONArray("modifiers").getJSONObject(i),true);
				
				if(bm != null)
				{
					this.modifiers.add(bm);
				}
			}
		}
		
		if(o.has("achievements"))
		{
			@SuppressWarnings("rawtypes")
            Iterator i=o.getJSONObject("achievements").keys();
			
			while(i.hasNext())
			{
				String a=(String) i.next();
				
				achievements.put(
					a,
					o.getJSONObject("achievements").getLong(a)
				);
			}
		}
		
		if(o.has("factions"))
		{
			JSONObject factions  =o.getJSONObject("factions");
			JSONArray  factionIds=factions.names();
			
			for(int i=0;i<factionIds.length();i++)
			{
				this.factions.put(
					Long    .parseLong(factionIds.getString(i)),
					factions.getInt   (factionIds.getString(i))
				);
			}
		}
	}

	@Override
	public JSONObject toJSONObject() throws JSONException
	{
		JSONObject o=new JSONObject();
		
		o.put("currentHealth",      currentHealth     );
		o.put("currentStamina",     currentStamina    );
		o.put("currentExperience",  currentExperience );
		o.put("lifetimeExperience", lifetimeExperience);
		
		o.put("advancements",       new JSONArray ()  );
		o.put("vmatrix",            new JSONArray ()  );
		o.put("itemsRecharging",    new JSONArray ()  );
		o.put("modifiers",          new JSONArray ()  );
		o.put("achievements",       new JSONObject()  );
		
		for(int i=0;i<this.vulnerabilityMatrix.length;i++)
		{
			o.getJSONArray("vmatrix").put(this.vulnerabilityMatrix[i]);
		}
		
		for(String s:advancements)
		{
			o.getJSONArray("advancements").put(s);
		}
		
		for(RechargingInstance ri:itemsRecharging)
		{
			o.getJSONArray("itemsRecharging").put(ri.toJSONObject());
		}
		
		for(BaseModifier m:modifiers)
		{
			o.getJSONArray("modifiers"      ).put( m.toJSONObject());
		}
		
		for(String a:achievements.keySet())
		{
			o.getJSONObject("achievements").put(a,achievements.get(a));
		}
		
		if(factions.size() > 0)
		{
			JSONObject f=new JSONObject();
			
			o.put("factions", f);
			
			for(Long l:factions.keySet())
			{
				f.put(
					String  .valueOf(l),
					factions.get    (l)
				);
			}
		}
		
		return o;
	}

    @Override
	public byte[] toBytes()
	{
		try
		{
		    ByteArrayOutputStream    baos=new ByteArrayOutputStream();
		    EnhancedDataOutputStream out =new EnhancedDataOutputStream(baos);

		    //CONSTANTS, BUT OH WELL :(
//		    out.writeDouble (DefaultCharacterStats.REGEN_INTERVAL  );
		    out.writeDouble (DefaultCharacterStats.ADV_COST_CEILING);
		    
		    out.writeInt    (baseHealthRegenRate);
		    out.writeInt    (baseStaminaRegenRate);
		    out.writeInt    (baseHealth);
		    out.writeInt    (baseStamina);
		    out.writeInt    (currentHealth);
		    out.writeInt    (currentStamina);
		    out.writeDouble (getMovementSpeed());
		    out.writeInt    (getMaximumHealth());
		    out.writeInt    (getMaximumStamina());
		    out.writeDouble (currentExperience);
		    out.writeDouble (lifetimeExperience);
		    
		    ////
		    Set<String> smks=statModifiers.keySet();
		    
		    smks=mergeWithEquipment(smks);
		    
		    out.writeShort(smks.size());
		    
		    for(String smk:smks)
		    {
		    	Object o=statModifiers.get(smk);
		    	
		    	if(o == null)
		    	{
		    		o=new Double(0.0);
		    	}
		    	
		    	out.writeUTF(smk);
		    	
		    	if(o instanceof Double)
		    	{
		    		out.writeDouble(((Double) o));
		    	}
		    	else if(o instanceof Long)
		    	{
		    		out.writeDouble(((Long  ) o));
		    	}
		    }
		    
		    
		    ////
			for(int i=0;i<this.vulnerabilityMatrix.length;i++)
			{
				out.writeDouble(this.vulnerabilityMatrix[i]);
			}

			
			////
		    out.writeShort(advancements.size());
			
			for(String s:advancements)
			{
				out.writeUTF(s);
			}

			////
		    out.writeShort(itemsRecharging.size());
			
			for(RechargingInstance ri:itemsRecharging)
			{
				out.write(ri.toBytes());
			}

			////
		    out.writeShort(modifiers.size());
		    
			for(BaseModifier m:modifiers)
			{
				out.write(m.toBytes());
			}
			
			////
			out.writeShort(achievements.size());
			
			for(String a:achievements.keySet())
			{
				out.writeUTF   (a                  );
				out.writeDouble(achievements.get(a));
			}
			
			////
			if(itemChargingAction != null)
			{
				out.writeBoolean(true);
				out.writeDouble (itemCharging[0]);
				out.writeDouble (itemCharging[1]);
				out.writeUTF    (itemChargingAction.getItem().getItemId());
			}
			else
			{
				out.writeBoolean(false);
			}

			////
			{
				out.writeShort(factions.size());
				
				for(Long l:factions.keySet())
				{
					out.writeDouble(l              );
					out.writeInt   (factions.get(l));
				}
			}
			
			out.close();
		    
		    return(baos.toByteArray());
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return(null);
	}

	private Set<String> mergeWithEquipment(Set<String> smks)
    {
	    Set<String> all=new TreeSet<String>();
	    
	    all.addAll(smks);
	    
		for(ItemInstance eq:character.getInventoryBlock(Character.EQUIPMENT_BLOCK))
		{
			try
			{
				if(eq != null && eq.getItemDef().getProps().has("effects"))
				{
					String[] ss=JSONObject.getNames(eq.getItemDef().getProps().getJSONObject("effects"));
					
					for(String s:ss)
					{
						all.add(s);
					}
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
	    
	    return all;
    }

	private double getVulnerabilityForIndex(int index)
	{
		if(index < 0 && index >= this.vulnerabilityMatrix.length)
		{
			return(1.0);
		}
		
		return(this.vulnerabilityMatrix[index]);
	}
	
	private double getVulnerabilityModifierForDefender(Character attacker, Character defender, Item weapon)
	{
		if(weapon.ignoreWeaponMatrices())
		{
			return(1.0);
		}
		
		int idx=defender.getOrientationModifierMatrixPosition(
			getMatrixIndex(
				defender.getLocation(),
				attacker.getLocation(),
				defender.getInstanceId(),
				weapon.getWeaponRange()
			)
		);
	
		if(idx != -1)
		{
			return(((DefaultCharacterStats) defender.getCharacterStats()).getVulnerabilityForIndex(idx));
		}
		
	    return(0.0);
	}
	
	private double getEffectivenessModifierForAttacker(Character attacker, Character defender, Item weapon)
	{
		if(weapon.ignoreWeaponMatrices())
		{
			return(1.0);
		}
		
		int idx=attacker.getOrientationModifierMatrixPosition(
			getMatrixIndex(
				attacker.getLocation(),
				defender.getLocation (),
				defender.getInstanceId(),
				weapon.getWeaponRange()
			)
		);
	
		if(idx != -1)
		{
			try
			{
				return(weapon.getWeaponDef().getJSONArray("ematrix").getDouble(idx));
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		
	    return(0.0);
	}
	
	private static int getMatrixIndex(Point origin, Point reference, long instanceId, int range)
	{
	    if(range > 1)
	    {
		    int ox=FastMath.wrap(origin   .getX(),State.instance().getWorld().getInstance(instanceId).getTerrain().getSize());
		    int oy=FastMath.wrap(origin   .getY(),State.instance().getWorld().getInstance(instanceId).getTerrain().getSize());
		    int rx=FastMath.wrap(reference.getX(),State.instance().getWorld().getInstance(instanceId).getTerrain().getSize());
		    int ry=FastMath.wrap(reference.getY(),State.instance().getWorld().getInstance(instanceId).getTerrain().getSize());
		    
		    if(rx == ox)
		    {
		    	if(ry > oy)
		    	{
		    		return(7);
		    	}
		    	else if(ry < oy)
		    	{
		    		return(1);
		    	}
		    }
		    else if(ry == oy)
		    {
		    	if(rx > ox)
		    	{
		    		return(5);
		    	}
		    	else if(rx < ox)
		    	{
		    		return(3);
		    	}
		    }
		    else if(rx > ox)
		    {
		    	if(ry > oy)
		    	{
		    		return(8);
		    	}
		    	else if(ry < oy)
		    	{
		    		return(2);
		    	}
		    }
		    else if(rx < ox)
		    {
		    	if(ry > oy)
		    	{
		    		return(6);
		    	}
		    	else if(ry < oy)
		    	{
		    		return(0);
		    	}
		    }
	    }
	    else
	    {
			int i=0;
			
		    for(int y=origin.getY()-1;y<=origin.getY()+1;y++)
		    {
			    for(int x=origin.getX()-1;x<=origin.getX()+1;x++)
			    {
			    	if(reference.equals(
				    	FastMath.wrap(x,State.instance().getWorld().getInstance(instanceId).getTerrain().getSize()),
				    	FastMath.wrap(y,State.instance().getWorld().getInstance(instanceId).getTerrain().getSize())
		    		))
			    	{
			    		return(i);
			    	}
			    	
			    	i++;
			    }
		    }
	    }
	    
	    return(-1);
	}

	public void updateStatModifier(int mode, String statModifierKey, Object statModifier)
    {
		if(statModifier instanceof Integer)
		{
			statModifier=new Long(((Integer) statModifier).longValue());
		}
		
		switch(mode)
		{
			case STAT_MODIFIER_MODE_ADD:
			{
				if(statModifiers.containsKey(statModifierKey))
				{
					if(statModifier instanceof Double)
					{
						statModifiers.put(statModifierKey,((Double) statModifier).doubleValue()+((Double) statModifiers.get(statModifierKey)).doubleValue());
					}
					else if(statModifier instanceof Long)
					{
						statModifiers.put(statModifierKey,((Long) statModifier).longValue()+((Long) statModifiers.get(statModifierKey)).longValue());
					}
					
					if(character != null)
					{
					    try
					    {
						    CommandProperties props=new CommandProperties();
						   
						    props.setCharacter("character",   character      );
						    props.setString   ("statModifier",statModifierKey);
					
							State.instance().getGame().addIncomingRequest(
								"CORE.COMMAND.CHARACTER.STAT.MODIFIER.CHANGE",
								props
							);
					    }
					    catch(CommandNotSupportedException e)
					    {
					    	e.printStackTrace();
					    }
					    catch(IsNotServerInvokedCommand e)
					    {
					    	e.printStackTrace();
					    }
					}
				}
				else
				{
					if(statModifier instanceof Double || statModifier instanceof Long)
					{
						statModifiers.put(statModifierKey,statModifier);
					}
					
					if(character != null)
					{
					    try
					    {
						    CommandProperties props=new CommandProperties();
						   
						    props.setCharacter("character",   character      );
						    props.setString   ("statModifier",statModifierKey);
					
							State.instance().getGame().addIncomingRequest(
								"CORE.COMMAND.CHARACTER.STAT.MODIFIER.CHANGE",
								props
							);
					    }
					    catch(CommandNotSupportedException e)
					    {
					    	e.printStackTrace();
					    }
					    catch(IsNotServerInvokedCommand e)
					    {
					    	e.printStackTrace();
					    }
					}
				}
				
				break;
			}
			case STAT_MODIFIER_MODE_SUBTRACT:
			{
				if(statModifiers.containsKey(statModifierKey))
				{
					if(statModifier instanceof Double)
					{
						statModifiers.put(statModifierKey,((Double) statModifiers.get(statModifierKey)).doubleValue()-((Double) statModifier).doubleValue());
						
						if(((Double) statModifiers.get(statModifierKey)).doubleValue() == 0.0)
						{
							statModifiers.remove(statModifierKey);
						}
					}
					else if(statModifier instanceof Long)
					{
						statModifiers.put(statModifierKey,((Long) statModifiers.get(statModifierKey)).longValue()-((Long) statModifier).longValue());
						
						if(((Long) statModifiers.get(statModifierKey)).longValue() == 0)
						{
							statModifiers.remove(statModifierKey);
						}
					}
					
					//NEED TO MOD HEALTH
					if(DefaultCharacterStats.STAT_KEY_HEALTH_INCREASE.equals(statModifierKey) && currentHealth > getMaximumHealth())
					{
						modifyHealth(currentHealth-getMaximumHealth(),ICharacterStats.HEALTH_MODIFY_CAUSE_MAXIMUM_HEALTH_RECALCULATED, null);
					}

					//NEED TO MOD STAMINA
					if(DefaultCharacterStats.STAT_KEY_STAMINA_INCREASE.equals(statModifierKey) && currentStamina > getMaximumStamina())
					{
						modifyStamina(currentStamina-getMaximumStamina(),ICharacterStats.STAMINA_MODIFY_CAUSE_MAXIMUM_STAMINA_RECALCULATED, null);
					}
					
					if(character != null)
					{
					    try
					    {
						    CommandProperties props=new CommandProperties();
						   
						    props.setCharacter("character",   character      );
						    props.setString   ("statModifier",statModifierKey);
					
							State.instance().getGame().addIncomingRequest(
								"CORE.COMMAND.CHARACTER.STAT.MODIFIER.CHANGE",
								props
							);
					    }
					    catch(CommandNotSupportedException e)
					    {
					    	e.printStackTrace();
					    }
					    catch(IsNotServerInvokedCommand e)
					    {
					    	e.printStackTrace();
					    }
					}
				}
				
				break;
			}
		}
    }
}