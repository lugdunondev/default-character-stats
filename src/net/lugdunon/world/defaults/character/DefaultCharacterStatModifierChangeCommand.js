Namespace.declare("net.lugdunon.world.defaults.character");

Namespace.newClass("net.lugdunon.world.defaults.character.DefaultCharacterStatModifierChangeCommand","net.lugdunon.command.core.Command");

// //

net.lugdunon.world.defaults.character.DefaultCharacterStatModifierChangeCommand.prototype.init=function(initData)
{
	this.callSuper(net.lugdunon.world.defaults.character.DefaultCharacterStatModifierChangeCommand,"init",[initData]);

	return(this);
};

net.lugdunon.world.defaults.character.DefaultCharacterStatModifierChangeCommand.prototype.opInit=function(initData)
{

};

net.lugdunon.world.defaults.character.DefaultCharacterStatModifierChangeCommand.prototype.getCommandLength=function()
{
    return(0);
};

net.lugdunon.world.defaults.character.DefaultCharacterStatModifierChangeCommand.prototype.buildCommand=function(dataView)
{

};

net.lugdunon.world.defaults.character.DefaultCharacterStatModifierChangeCommand.prototype.commandResponse=function(res)
{
	var statModifier=res.readString();
	var char        =game.getCharacter(res.readUint8()==0?res.readFloat64():res.readString());

	if(char != null)
	{
		char.characterStats.updateStatModifier(statModifier,res.readBoolean()===false?false:res.readFloat64());
	}
};