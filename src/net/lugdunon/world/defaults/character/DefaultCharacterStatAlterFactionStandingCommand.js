Namespace.declare("net.lugdunon.world.defaults.character");

Namespace.newClass("net.lugdunon.world.defaults.character.DefaultCharacterStatAlterFactionStandingCommand","net.lugdunon.command.core.Command");

// //

net.lugdunon.world.defaults.character.DefaultCharacterStatAlterFactionStandingCommand.prototype.init=function(initData)
{
	this.callSuper(net.lugdunon.world.defaults.character.DefaultCharacterStatAlterFactionStandingCommand,"init",[initData]);

	return(this);
};

net.lugdunon.world.defaults.character.DefaultCharacterStatAlterFactionStandingCommand.prototype.opInit=function(initData)
{

};

net.lugdunon.world.defaults.character.DefaultCharacterStatAlterFactionStandingCommand.prototype.getCommandLength=function()
{
    return(0);
};

net.lugdunon.world.defaults.character.DefaultCharacterStatAlterFactionStandingCommand.prototype.buildCommand=function(dataView)
{

};

net.lugdunon.world.defaults.character.DefaultCharacterStatAlterFactionStandingCommand.prototype.commandResponse=function(res)
{
	var char=game.getCharacter(res.readUint8()==0?res.readFloat64():res.readString());

	if(char != null)
	{
		char.characterStats.alterFactionStanding(res.readFloat64(),res.readInt32(),res.readInt32());
	}
};