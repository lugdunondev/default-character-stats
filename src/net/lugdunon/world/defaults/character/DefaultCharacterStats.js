Namespace.declare("net.lugdunon.world.defaults.character");
Namespace.require("net.lugdunon.state.character.stats.ICharacterStats");
Namespace.require("net.lugdunon.item.RechargingInstance");
Namespace.require("net.lugdunon.ui.achievements.AchievementsDialog");
Namespace.newClass("net.lugdunon.world.defaults.character.DefaultCharacterStats","net.lugdunon.state.character.stats.ICharacterStats");

// //

net.lugdunon.world.defaults.character.DefaultCharacterStats.prototype.init=function(initData)
{
	this.callSuper(net.lugdunon.world.defaults.character.DefaultCharacterStats,"init",[initData]);

	var i;
	var l;
	
	this.characterStatsPane  =null;
	this.healthGauge         =null;
	this.character           =initData.character;

//	this.regenInterval       =initData.dataView.readFloat64();
	this.advCostCeiling      =initData.dataView.readFloat64();
	
	this.baseHealthRegenRate =initData.dataView.readUint32();
	this.baseStaminaRegenRate=initData.dataView.readUint32();
	
	this.baseHealth          =initData.dataView.readUint32();
	this.baseStamina         =initData.dataView.readUint32();
	
	this.currentHealth       =initData.dataView.readUint32();
	this.currentStamina      =initData.dataView.readUint32();

	this.movementSpeed       =initData.dataView.readFloat64();
	
	this.maximumHealth       =initData.dataView.readUint32();
	this.maximumStamina      =initData.dataView.readUint32();
	
	this.currentExperience   =initData.dataView.readFloat64();
	this.lifetimeExperience  =initData.dataView.readFloat64();

	////
	this.statModifiers={};
	l=initData.dataView.readUint16();
	
	for(i=0;i<l;i++)
	{
		this.statModifiers[initData.dataView.readString()]=initData.dataView.readFloat64();
	}
	
	////
	this.vulnerabilityMatrix=[];
	
	for(i=0;i<9;i++)
	{
		this.vulnerabilityMatrix[i]=initData.dataView.readFloat64();
	}
	
	
	////
	this.advancements=[];
	
	l=initData.dataView.readUint16();
	
	for(i=0;i<l;i++)
	{
		this.advancements[i]=initData.dataView.readString();
	}
	
	////
	this.itemsRecharging=[];

	l=initData.dataView.readUint16();

	for(i=0;i<l;i++)
	{
		this.itemsRecharging.push(
			new net.lugdunon.item.RechargingInstance().init(initData.dataView)
		);
	}
	
	////
	this.modifiers=[];

	l=initData.dataView.readUint16();

	for(i=0;i<l;i++)
	{
		try
		{
			this.applyModifier(Namespace.instantiate(initData.dataView.readString()).init({characterStats:this,dataView:initData.dataView}));
		}
		catch(e)
		{
			Namespace.error(e,null,this);
		}
	}
	
	////
	this.achievementNames=[];
	this.achievements    ={};
	l=initData.dataView.readUint16();
	
	for(i=0;i<l;i++)
	{
		this.achievementNames.push(initData.dataView.readString());
		this.achievements[this.achievementNames.last()]=initData.dataView.readFloat64();
	}
	
	this.achievementNames.sort();
	
	////
	this.itemCharging=null;
	
	if(initData.dataView.readBoolean())
	{
		this.itemCharging=[
	       	initData.dataView.readFloat64(),
	    	initData.dataView.readFloat64(),
	    	initData.dataView.readString ()
	    ];
	}
	
	////
	var factionCount=initData.dataView.readUint16();

	this.factions={};
	
	for(var i=0;i<factionCount;i++)
	{
		this.factions[initData.dataView.readFloat64()]=initData.dataView.readInt32();
	}

	this.createCharacterStats();
	
	return(this);
};

net.lugdunon.world.defaults.character.DefaultCharacterStats.prototype.getSchoolBonus=function(applier, school)
{
	var amount=0.0;
	
	if(applier.characterStats.statModifiers["spellSchoolBonus_"+school])
	{
		amount+=applier.characterStats.statModifiers["spellSchoolBonus_"+school];
	}
	
	return(amount);
}

net.lugdunon.world.defaults.character.DefaultCharacterStats.prototype.getValueForItem=function(baseValue,itemType,itemSubType,itemSpellSchool)
{
	var bonus=0.0;
	
	baseValue=$.extend({},baseValue);
	
	if(itemSpellSchool == null)
	{
		if(this.statModifiers["damageBonus"])
		{
			bonus+=this.statModifiers["damageBonus"];
		}
		
		if(itemType != null && this.statModifiers["damageBonus_"+itemType])
		{
			bonus+=this.statModifiers["damageBonus_"+itemType];
		}
	}
	else if(this.statModifiers["spellSchoolBonus_"+itemSpellSchool])
	{
		bonus+=this.statModifiers["spellSchoolBonus_"+itemSpellSchool];
	}
	
	if(baseValue.exact != null)
	{
		baseValue.exact+=Math.ceil(baseValue.exact*bonus);
	}
	else
	{
		baseValue.min  +=Math.ceil(baseValue.min  *bonus);
		baseValue.max  +=Math.ceil(baseValue.max  *bonus);
	}
	
	return(baseValue);
};

net.lugdunon.world.defaults.character.DefaultCharacterStats.prototype.getMovementSpeed =function()
{
	if(this.statModifiers["movementSpeedIncrease"])
	{
		return(1.0/(1.0+this.statModifiers["movementSpeedIncrease"]));
	}
	
	return(this.movementSpeed);
};

net.lugdunon.world.defaults.character.DefaultCharacterStats.prototype.getCurrentHealth =function(){return(this.currentHealth );};
net.lugdunon.world.defaults.character.DefaultCharacterStats.prototype.getMaximumHealth =function(){return(this.maximumHealth );};

net.lugdunon.world.defaults.character.DefaultCharacterStats.prototype.getCurrentStamina=function(){return(this.currentStamina);};
net.lugdunon.world.defaults.character.DefaultCharacterStats.prototype.getMaximumStamina=function(){return(this.maximumStamina);};

net.lugdunon.world.defaults.character.DefaultCharacterStats.prototype.update                    =function(delta)
{
	var context=this;
	var i;
	
	for(i=0;i<this.itemsRecharging.length;i++)
	{
		this.itemsRecharging[i].accumulatedDelta+=delta;
		
		if(this.itemsRecharging[i].accumulatedDelta >= this.itemsRecharging[i].duration)
		{
			this.itemsRecharging.splice(i--,1);
		}
	}
	
	for(i=0;i<this.modifiers.length;i++)
	{
		this.modifiers[i].update(delta);
		
		if(this.modifiers[i].expired())
		{
			this.modifiers.splice(i--,1);
		}
	}
	
	if(this.itemCharging != null)
	{
		this.itemCharging[0]+=delta;
		
		if(this.itemCharging[0] > this.itemCharging[1])
		{
			this.itemCharging=null;
		}
	}
};

net.lugdunon.world.defaults.character.DefaultCharacterStats.prototype.getItemChargeStatus  =function(itemDefId)
{
	if((this.itemCharging != null) && ((itemDefId == null) || (this.itemCharging[2] == itemDefId)))
	{
		return(this.itemCharging);
	}
	
	return(null);
};

net.lugdunon.world.defaults.character.DefaultCharacterStats.prototype.itemChargeInvoked    =function(itemDefId,charge)
{
	this.itemCharging=[0,charge,itemDefId];
};

net.lugdunon.world.defaults.character.DefaultCharacterStats.prototype.itemChargeInterrupted=function(itemDefId)
{
	this.itemCharging=null;
};

net.lugdunon.world.defaults.character.DefaultCharacterStats.prototype.getItemRechargeStatus=function(item)
{
	var ir=null;
	
	for(var i=0;i<this.itemsRecharging.length;i++)
	{
		if(this.itemsRecharging[i].matches(item))
		{
			if(
				(ir == null) || 
				(ir.duration < this.itemsRecharging[i].duration) ||
				(
					(ir.duration        == this.itemsRecharging[i].duration       ) &&
					(ir.acumulatedDelta <  this.itemsRecharging[i].acumulatedDelta)
				)
			)
			{
				ir=this.itemsRecharging[i];
			}
		}
	}
	
	return(ir);
};

//TODO: VERIFY
net.lugdunon.world.defaults.character.DefaultCharacterStats.prototype.itemRechargeInvoked  =function(item,recharge)
{
	var duration=(recharge+(game.getLatency()*2))*net.lugdunon.CHARGE_ADJUSTMENT;
	
	if(duration > 8640000000)
	{
		Namespace.warn("RECHARGE ERROR: "+recharge+" "+game.getLatency()+" "+net.lugdunon.CHARGE_ADJUSTMENT,null,this);
		
		if(recharge > 8640000000)
		{
			recharge=2000;
		}
		
		duration=recharge;
	}

	this.itemsRecharging.push(new net.lugdunon.item.RechargingInstance().init({item:item,duration:duration}));
};

net.lugdunon.world.defaults.character.DefaultCharacterStats.prototype.initStatsText             =function()
{
	var context=this;
	
	$.each(
		game.characterStats.modifiers,
		function( key, value )
		{
			context[value.textRenderer].call(context,key);
		}
	);
	
	for(var i=0;i<game.craftingDisciplineIds.length;i++)
	{
		this.setCraftingLevelText(game.craftingDisciplineIds[i]);
	}
};

net.lugdunon.world.defaults.character.DefaultCharacterStats.prototype.getAdvancementCost        =function(advancementId)
{
	return(Math.pow(2,this.advancements.length>this.advCostCeiling?this.advCostCeiling:this.advancements.length));
};

net.lugdunon.world.defaults.character.DefaultCharacterStats.prototype.getAdvancement            =function(advancementId)
{
	if(game.advancements[advancementId])
	{
		return(game.advancements[advancementId]);
	}
	
	var adv =advancementId.split(".LEVEL");
	var cAdv=game.advancements[adv[0]+".LEVEL0"];
	
	while(true)
	{
		cAdv=cAdv.child;
		
		if(cAdv == null)
		{
			break;
		}
		
		if(cAdv.id == advancementId)
		{
			return(cAdv);
		}
	}
	
	return(null);
};

net.lugdunon.world.defaults.character.DefaultCharacterStats.prototype.setCurrentHealth=function(currentHealth)
{
	if(this.currentHealth != currentHealth)
	{
		var amount=currentHealth-this.currentHealth;
		
		this.currentHealth=currentHealth;
	
		if(this.healthGauge != null)
		{
			if(this.maximumHealth > 0)
			{
				this.healthGauge.update(this.currentHealth/this.maximumHealth);
			}
			else
			{
				this.healthGauge.update(-1);
			}
		}

		if(!this.character.noEffect)
		{
			this.character.applyEffect(
				amount>0?
				new net.lugdunon.character.effect.HealedEffect ().init(Math.abs(amount)):
				new net.lugdunon.character.effect.DamagedEffect().init(Math.abs(amount))
			);
		}

		this.renderStatText("health");
	}
};

net.lugdunon.world.defaults.character.DefaultCharacterStats.prototype.setMaximumHealth=function(maximumHealth)
{
	if(this.maximumHealth != maximumHealth)
	{
		this.maximumHealth=maximumHealth;
		
		if(this.healthGauge != null)
		{
			if(this.maximumHealth > 0)
			{
				this.healthGauge.update(this.currentHealth/this.maximumHealth);
			}
			else
			{
				this.healthGauge.update(-1);
			}
		}

		this.renderStatText("health");
	}
};

net.lugdunon.world.defaults.character.DefaultCharacterStats.prototype.setCurrentStamina=function(currentStamina)
{
	this.currentStamina=currentStamina;
	this.renderStatText("stamina");
};

net.lugdunon.world.defaults.character.DefaultCharacterStats.prototype.setMaximumStamina=function(maximumStamina)
{
	this.maximumStamina=maximumStamina;
	this.renderStatText("stamina");
};

net.lugdunon.world.defaults.character.DefaultCharacterStats.prototype.getHealthRegenRate =function()
{
	return(this.healthRegenRate);
};

net.lugdunon.world.defaults.character.DefaultCharacterStats.prototype.getStaminaRegenRate=function()
{
	return(this.staminaRegenRate);
};

net.lugdunon.world.defaults.character.DefaultCharacterStats.prototype.setCurrentExperience =function(currentExperience)
{
	if(currentExperience-this.currentExperience != 0)
	{
		this.character.applyEffect(
			new net.lugdunon.character.effect.ExperienceEffect().init(currentExperience-this.currentExperience)
		);
		
		if((this.character == game.player) && ((currentExperience-this.currentExperience) > 0))
		{
			game.console.log("You gained "+(currentExperience-this.currentExperience)+" exp!","#0F0","experienceGained",true);
		}
	}
	
	this.currentExperience =currentExperience;
	this.renderSimpleText("currentExperience");

	if((this.character == game.player) && game.getCurrentGameState().currentDialog != null && Namespace.instanceOf(game.getCurrentGameState().currentDialog,"net.lugdunon.ui.advancements.AdvancementsDialog"))
	{
		game.getCurrentGameState().currentDialog.advCatList.find(".advancementsExperienceOnhand").html(
			"Avail XP: "+game.player.characterStats.currentExperience
		);
	}
};

net.lugdunon.world.defaults.character.DefaultCharacterStats.prototype.setLifetimeExperience=function(lifetimeExperience)
{
	this.lifetimeExperience=lifetimeExperience;
	this.renderSimpleText("lifetimeExperience");
};

net.lugdunon.world.defaults.character.DefaultCharacterStats.prototype.addAdvancement   =function(advancement)
{
	this.advancements.push(advancement);
	
	this.renderAdvancementsText("advancement");
	
	for(var i=0;i<game.craftingDisciplineIds.length;i++)
	{
		this.setCraftingLevelText(game.craftingDisciplineIds[i]);
	}
};

net.lugdunon.world.defaults.character.DefaultCharacterStats.prototype.removeAdvancement=function(advancement)
{
	this.advancements.remove(advancement);
	
	this.renderAdvancementsText("advancement");
	
	for(var i=0;i<game.craftingDisciplineIds.length;i++)
	{
		this.setCraftingLevelText(game.craftingDisciplineIds[i]);
	}
};

net.lugdunon.world.defaults.character.DefaultCharacterStats.prototype.updateStatModifier=function(statModifier,val)
{
	if(val === false)
	{
		this.statModifiers[statModifier]=null;
		delete this.statModifiers[statModifier];
	}
	else
	{
		this.statModifiers[statModifier]=val;
	}

	if(game.characterStats.modifiers[statModifier])
	{
		this[game.characterStats.modifiers[statModifier].textRenderer].call(this,statModifier);
	}
};

net.lugdunon.world.defaults.character.DefaultCharacterStats.prototype.getCraftingLevel=function(craftType)
{
	if(this.statModifiers["CRAFT_"+craftType])
	{
		return(this.statModifiers["CRAFT_"+craftType]);
	}
	
	return(0);
};

net.lugdunon.world.defaults.character.DefaultCharacterStats.prototype.awardAchievement=function(achievementId,ts)
{
	try
	{
		if(this.getAchievementAwarded(achievementId) == net.lugdunon.state.character.stats.ICharacterStats.ACHIEVEMENT_UNAWARDED)
		{
			this.achievementNames.push(achievementId);
			this.achievements[achievementId]=ts;
			
			this.achievementNames.sort();
	
			if(game.player == this.character)
			{
				game.console.log("Achievement Earned: "+game.achievements[achievementId].name,"#0F0","achievement",false);
				
				//SHOW ACHIEVEMENT NOTIFICATION
				net.lugdunon.ui.achievements.AchievementsDialog.showAchievement(game.achievements[achievementId]);
				
				//TODO: UPDATE DIALOG IF OPEN
			}
			else
			{
				game.console.log(this.character.name+" Earned Achievement: "+game.achievements[achievementId].name,"#0F0","achievement",false);
			}
		}
	}
	catch(e)
	{
		console.log(e);
	}
};

net.lugdunon.world.defaults.character.DefaultCharacterStats.prototype.listAchievementsAwarded=function()
{
	return(this.achievementNames);
};

net.lugdunon.world.defaults.character.DefaultCharacterStats.prototype.getAchievementAwarded=function(achievementId)
{
	if(this.achievements[achievementId] != null)
	{
		return(this.achievements[achievementId]);
	}
	
	return(net.lugdunon.state.character.stats.ICharacterStats.ACHIEVEMENT_UNAWARDED);
};

net.lugdunon.world.defaults.character.DefaultCharacterStats.prototype.getWeaponCharge=function(weapon)
{
	return(weapon.props.weapon.charge);
};

net.lugdunon.world.defaults.character.DefaultCharacterStats.prototype.getToolCharge  =function(tool)
{
	return(tool.props.tool.charge);
};

net.lugdunon.world.defaults.character.DefaultCharacterStats.prototype.getWeaponRecharge=function(weapon)
{
	return(weapon.props.weapon.recharge);
}

net.lugdunon.world.defaults.character.DefaultCharacterStats.prototype.getToolRecharge  =function(tool)
{
	return(tool.props.tool.recharge);
}

net.lugdunon.world.defaults.character.DefaultCharacterStats.prototype.getCharacterListItemHTML=function()
{
	return(
		"<div class='characterName'>"+this.character.name+" - Exp: "+this.currentExperience+"/"+this.lifetimeExperience+", Adv: "+this.advancements.length+" of "+game.advancementLength+"</div>"+
		"<div class='characterLastPlayed'>Created On : "+this.getDate(new Date(this.character.createdOn))+"</div>"+
		"<div class='characterTotalPlayed'>Last Played On: "+this.getDate(new Date(this.character.lastPlayed))+"</div>"
	);
};

net.lugdunon.world.defaults.character.DefaultCharacterStats.prototype.populateCharacterStats=function(characterStatsContainerEl)
{
	characterStatsContainerEl.html(this.characterStatsPane);
	
	$.each(
		this.characterStatsPane.find(".characterStatsSectionExpanded"),
		function(i,e)
		{
			$(e).css("height",(24+$(e).children(":last").height())+"px");
		}
	);
	
	this.characterStatsPane.find(".cssToggleElement").off("click");
	this.characterStatsPane.find(".cssToggleElement").on ("click",
		function(e)
		{
			if($(this).parent().hasClass("characterStatsSectionExpanded"))
			{
				$(this).parent().removeClass("characterStatsSectionExpanded");
				$(this).parent().css("height",(24)+"px");
			}
			else
			{
				$(this).parent().addClass("characterStatsSectionExpanded");
				$(this).parent().css("height",(24+$(this).parent().children(":last").height())+"px");
			}
		}
	);
};

net.lugdunon.world.defaults.character.DefaultCharacterStats.prototype.createCharacterStats=function()
{
	var section;
	
	this.characterStatsPane=$("<div class='characterStatsInner'></div>");
		
	for(var i=0;i<game.characterStats.panels.length;i++)
	{
		section=$(
		  "<div class='characterStatsSection characterStatsSectionExpanded'>"+
		    "<div class='cssToggleElement'></div>"+
		    "<div class='cssTitle'>"+game.characterStats.panels[i].name+"</div>"+
		    "<div class='cssBody' name='"+game.characterStats.panels[i].id+"'>"+
		    "</div>"+
		  "</div>"
		);
	
		for(var j=0;j<game.characterStats.panels[i].contents.length;j++)
		{
			section.find("[name="+game.characterStats.panels[i].id+"]").append(
				"<div class='cssLabel'>"+
				game.characterStats.modifiers[game.characterStats.panels[i].contents[j]].name+
				"</div><div name='"+
				game.characterStats.panels[i].contents[j]+
				"' class='cssValue'>&nbsp;</div>"
			);
		}
		
		this.characterStatsPane.append(section);
	}
	
	for(var i=0;i<game.craftingDisciplineIds.length;i++)
	{
		this.characterStatsPane.find("[name=crafting]").append(
			"<div class='cssLabel'>"+
			game.craftingDisciplines[game.craftingDisciplineIds[i]].name+
			"</div><div name='CRAFTING_"+
			game.craftingDisciplineIds[i]+
			"' class='cssValue'>&nbsp;</div>"
		);
	}
};

net.lugdunon.world.defaults.character.DefaultCharacterStats.prototype.getDate=function(date)
{
	var m=(date.getMonth()+1);
	var d=date.getDate();

	m=m<10?"0"+m:m;
	d=d<10?"0"+d:d;
	
	return(m+"/"+d+"/"+date.getFullYear());//+" "+date.getHours()+":"+date.getMinutes());
};

net.lugdunon.world.defaults.character.DefaultCharacterStats.prototype.initGauge=function()
{
	this.healthGauge=new net.lugdunon.ui.LabelGauge().init(
		{
			label:this.character.sprite.label,
			value:this.currentHealth/this.maximumHealth
		}
	);
};

net.lugdunon.world.defaults.character.DefaultCharacterStats.prototype.drawGauge=function(delta,x,y)
{
	this.healthGauge.draw(
		game.getContext(),
		delta,
		Math.floor((this.character.sprite.labelOffset().x-(52                                    ))+x   ),
		Math.floor((this.character.sprite.labelOffset().y-(this.character.sprite.label.height()/2))+y+20)
	);
};

net.lugdunon.world.defaults.character.DefaultCharacterStats.prototype.draw=function(delta)
{
	for(i=0;i<this.modifiers.length;i++)
	{
		this.modifiers[i].draw(delta);
	}
};

////

net.lugdunon.world.defaults.character.DefaultCharacterStats.prototype.renderStatText=function(id)
{
	var ms=(id=="stamina")?this.maximumStamina:this.maximumHealth;
	var bs=(id=="stamina")?this.baseStamina   :this.baseHealth;
	var cs=(id=="stamina")?this.currentStamina:this.currentHealth;
	
	if(this.character == game.player)
	{
		var s=ms;
		
		if(s > bs)
		{
			s="<font color='#88F'>"+s+"</font> <font color='#0B0'>+"+(s-bs)+"</font>";
		}
		else if(s < bs)
		{
			s="<font color='#88F'>"+s+"</font> <font color='#B00'>-"+(bs-s)+"</font>";
		}
		
		this.characterStatsPane.find("[name="+id+"]").html(cs+"/"+s);
	}
};

net.lugdunon.world.defaults.character.DefaultCharacterStats.prototype.renderRegenText=function(id)
{
	var brr=(id=="staminaRegenIncrease")?this.baseStaminaRegenRate              :this.baseHealthRegenRate;
	var bri=(id=="staminaRegenIncrease")?this.statModifiers.staminaRegenIncrease:this.statModifiers.healthRegenIncrease;
	
	if(this.character == game.player)
	{
		var regen=brr+((bri==null)?(0):(bri));
		
		if(regen > brr)
		{
			regen="<font color='#88F'>"+regen+"</font> <font color='#0B0'>+"+(regen-brr)+"</font>";
		}
		else if(regen < brr)
		{
			regen="<font color='#88F'>"+regen+"</font> <font color='#B00'>-"+(brr-regen)+"</font>";
		}
		else
		{
			regen=brr;
		}
		
		this.characterStatsPane.find("[name="+id+"]").html(regen);
	}
};

net.lugdunon.world.defaults.character.DefaultCharacterStats.prototype.renderSimpleText=function(id)
{
	if(this.character == game.player)
	{
		this.characterStatsPane.find("[name="+id+"]").html(this[id]);
	}
};

net.lugdunon.world.defaults.character.DefaultCharacterStats.prototype.renderAdvancementsText=function(id)
{
	if(this.character == game.player)
	{
		this.characterStatsPane.find("[name=advancements]").html(this.advancements.length+" of "+game.advancementLength);
	}
};

net.lugdunon.world.defaults.character.DefaultCharacterStats.prototype.renderSimplePercentageText=function(id)
{
	if(this.character == game.player)
	{
		var dmgBonus=(((this.statModifiers[id]==null)?(0.0):(this.statModifiers[id]))*100).toFixed(0);
		
		if(dmgBonus > 0)
		{
			dmgBonus="<font color='#0B0'>+"+dmgBonus+"%</font>";
		}
		else if(dmgBonus < 0)
		{
			dmgBonus="<font color='#B00'>-"+dmgBonus+"%</font>";
		}
		
		this.characterStatsPane.find("[name="+id+"]").html(dmgBonus);
	}
};

net.lugdunon.world.defaults.character.DefaultCharacterStats.prototype.setCraftingLevelText=function(craftType)
{
	if(this.character == game.player)
	{
		if(this.advancements.contains("CRAFTING.SKILL."+craftType+".LEVEL2"))
		{
			this.characterStatsPane.find("[name=CRAFTING_"+craftType+"]").html("Master");
		}
		else if(this.advancements.contains("CRAFTING.SKILL."+craftType+".LEVEL1"))
		{
			this.characterStatsPane.find("[name=CRAFTING_"+craftType+"]").html("Journeyman");
		}
		else if(this.advancements.contains("CRAFTING.SKILL."+craftType+".LEVEL0"))
		{
			this.characterStatsPane.find("[name=CRAFTING_"+craftType+"]").html("Apprentice");
		}
		else
		{
			this.characterStatsPane.find("[name=CRAFTING_"+craftType+"]").html("<font color='#B00'>Unskilled</font>");
		}
	}
};

net.lugdunon.world.defaults.character.DefaultCharacterStats.prototype.applyModifier=function(modifier)
{
	for(var i=0;i<this.modifiers.length;i++)
	{
		if(this.modifiers[i].id==modifier.id)
		{
			this.modifiers[i].expire();
			this.modifiers.splice(i,1);
			break;
		}
	}

	this.modifiers.push(modifier);
};

net.lugdunon.world.defaults.character.DefaultCharacterStats.prototype.expireModifier=function(modifierId)
{
	for(var i=0;i<this.modifiers.length;i++)
	{
		if(this.modifiers[i].id==modifierId)
		{
			this.modifiers[i].expire();
			return;
		}
	}
};

net.lugdunon.world.defaults.character.DefaultCharacterStats.prototype.hasEncounteredFaction=function(factionId)
{
	return(this.factions[factionId] != null);
};

net.lugdunon.world.defaults.character.DefaultCharacterStats.prototype.alterFactionStanding=function(factionId,amount,standing)
{
	var prevFactionStanding=this.getFactionStanding   (factionId);
	var prevFactionLevel   =this.hasEncounteredFaction(factionId)?this.getFactionLevel(factionId):null;
	var factionLevel       =null;
	var isPlayer           =this.character == game.player;
	
	this.factions[factionId]=standing;
	
	//LOG CHANGE
	game.console.log(
		((isPlayer)?(this.character.name+" has "):("You have "))+
		((amount>0)?"gained":"lost")+" "+Math.abs(amount)+
		" reputation with the "+
		game.politics.getFaction(factionId).name,((amount>0)?"#0F0":"#F00"),
		"faction",
		isPlayer
	);
	
	//LOG IF FACTION STANDING CHANGES LEVELS
	{
		factionLevel=this.getFactionLevel(factionId);
		
		if(prevFactionLevel != factionLevel)
		{
			game.console.log(
				((isPlayer)?(this.character.name+"'"+((this.character.name.endsWith("s"))?(" "):("s "))):("Your "))+
				"reuptation level with the "+game.politics.getFaction(factionId).name+
				" is now '"+this.getFactionLevelName(factionLevel)+"'",
				((factionLevel>=net.lugdunon.state.character.stats.ICharacterStats.FACTION_LEVEL_NEUTRAL)?"#0F0":"#F00"),
				"faction",
				isPlayer
			);
		}
	}
	
	return(0);
};

net.lugdunon.world.defaults.character.DefaultCharacterStats.prototype.getFactionStanding=function(factionId)
{
	if(!this.hasEncounteredFaction(factionId))
	{
		return(game.politics.getFaction(factionId).initialStanding);
	}
	
	return(this.factions[factionId]);
};

net.lugdunon.world.defaults.character.DefaultCharacterStats.prototype.getFactionLevel=function(factionId)
{
	var fs=this.getFactionStanding(factionId);

	if(fs > 8250)
	{
		return(net.lugdunon.state.character.stats.ICharacterStats.FACTION_LEVEL_EXALTED);
	}
	else if(fs <= 8250 && fs > 3250)
	{
		return(net.lugdunon.state.character.stats.ICharacterStats.FACTION_LEVEL_REVERED);
	}
	else if(fs <= 3250 && fs > 1250)
	{
		return(net.lugdunon.state.character.stats.ICharacterStats.FACTION_LEVEL_HONORED);
	}
	else if(fs <= 1250 && fs > 250)
	{
		return(net.lugdunon.state.character.stats.ICharacterStats.FACTION_LEVEL_FRIENDLY);
	}
	else if(fs <= 250 && fs > -250)
	{
		return(net.lugdunon.state.character.stats.ICharacterStats.FACTION_LEVEL_NEUTRAL);
	}
	else if(fs <= -250 && fs > -1250)
	{
		return(net.lugdunon.state.character.stats.ICharacterStats.FACTION_LEVEL_UNFRIENDLY);
	}
	else if(fs <= -1250 && fs > -3250)
	{
		return(net.lugdunon.state.character.stats.ICharacterStats.FACTION_LEVEL_SUSPECT);
	}
	else if(fs <= -3250 && fs > -8250)
	{
		return(net.lugdunon.state.character.stats.ICharacterStats.FACTION_LEVEL_HOSTILE);
	}
	else //if(fs <= -8250)
	{
		return(net.lugdunon.state.character.stats.ICharacterStats.FACTION_LEVEL_HATED);
	}
};

net.lugdunon.world.defaults.character.DefaultCharacterStats.prototype.getFactionLevelPercentage=function(factionId)
{
	var fs=this.getFactionStanding(factionId);
	var fl=this.getFactionLevel   (factionId);

	switch(fl)
	{
		case net.lugdunon.state.character.stats.ICharacterStats.FACTION_LEVEL_EXALTED:
		{
			if(fs > 16250)
			{
				fs=16250;
			}
			
			return((fs-8250)/8000);
		}
		case net.lugdunon.state.character.stats.ICharacterStats.FACTION_LEVEL_REVERED:
		{
			return((fs-3250)/5000);
		}
		case net.lugdunon.state.character.stats.ICharacterStats.FACTION_LEVEL_HONORED:
		{
			return((fs-1250)/2000);
		}
		case net.lugdunon.state.character.stats.ICharacterStats.FACTION_LEVEL_FRIENDLY:
		{
			return((fs- 250)/1000);
		}
		case net.lugdunon.state.character.stats.ICharacterStats.FACTION_LEVEL_NEUTRAL:
		{
			return((fs+ 250)/ 500);
		}
		case net.lugdunon.state.character.stats.ICharacterStats.FACTION_LEVEL_UNFRIENDLY:
		{
			return((fs+1250)/1000);
		}
		case net.lugdunon.state.character.stats.ICharacterStats.FACTION_LEVEL_SUSPECT:
		{
			return((fs+3250)/2000);
		}
		case net.lugdunon.state.character.stats.ICharacterStats.FACTION_LEVEL_HOSTILE:
		{
			return((fs+8250)/5000);
		}
		case net.lugdunon.state.character.stats.ICharacterStats.FACTION_LEVEL_HATED:
		{
			if(fs < -16250)
			{
				fs=-16250;
			}
			
			return((fs+16250)/8000);
		}
	}
};

net.lugdunon.world.defaults.character.DefaultCharacterStats.prototype.getBuyModifierForFactionLevel  =function(factionLevel      )
{
	switch(factionLevel)
	{
		case net.lugdunon.state.character.stats.ICharacterStats.FACTION_LEVEL_EXALTED:
		{
			return(0.50);
		}
		case net.lugdunon.state.character.stats.ICharacterStats.FACTION_LEVEL_REVERED:
		{
			return(0.75);
		}
		case net.lugdunon.state.character.stats.ICharacterStats.FACTION_LEVEL_HONORED:
		{
			return(0.80);
		}
		case net.lugdunon.state.character.stats.ICharacterStats.FACTION_LEVEL_FRIENDLY:
		{
			return(0.95);
		}
		case net.lugdunon.state.character.stats.ICharacterStats.FACTION_LEVEL_NEUTRAL:
		{
			return(1.00);
		}
		case net.lugdunon.state.character.stats.ICharacterStats.FACTION_LEVEL_UNFRIENDLY:
		{
			return(1.50);
		}
		case net.lugdunon.state.character.stats.ICharacterStats.FACTION_LEVEL_SUSPECT:
		{
			return(1.75);
		}
		case net.lugdunon.state.character.stats.ICharacterStats.FACTION_LEVEL_HOSTILE:
		{
			return(2.00);
		}
		case net.lugdunon.state.character.stats.ICharacterStats.FACTION_LEVEL_HATED:
		{
			return(2.00);
		}
	}
	
	return(1.0);
};

net.lugdunon.world.defaults.character.DefaultCharacterStats.prototype.getFactionStandingRangeForLevel=function(factionLevel)
{
	switch(factionLevel)
	{
		case net.lugdunon.state.character.stats.ICharacterStats.FACTION_LEVEL_EXALTED:
		{
			return([  8250, 16000]);
		}
		case net.lugdunon.state.character.stats.ICharacterStats.FACTION_LEVEL_REVERED:
		{
			return([  3250,  8250]);
		}
		case net.lugdunon.state.character.stats.ICharacterStats.FACTION_LEVEL_HONORED:
		{
			return([  1250,  3250]);
		}
		case net.lugdunon.state.character.stats.ICharacterStats.FACTION_LEVEL_FRIENDLY:
		{
			return([   250,  1250]);
		}
		case net.lugdunon.state.character.stats.ICharacterStats.FACTION_LEVEL_NEUTRAL:
		{
			return([  -250,   250]);
		}
		case net.lugdunon.state.character.stats.ICharacterStats.FACTION_LEVEL_UNFRIENDLY:
		{
			return([ -1250,  -250]);
		}
		case net.lugdunon.state.character.stats.ICharacterStats.FACTION_LEVEL_SUSPECT:
		{
			return([ -3250, -1250]);
		}
		case net.lugdunon.state.character.stats.ICharacterStats.FACTION_LEVEL_HOSTILE:
		{
			return([ -8250, -3250]);
		}
		case net.lugdunon.state.character.stats.ICharacterStats.FACTION_LEVEL_HATED:
		{
			return([-16000, -8250]);
		}
	}
	
	return([0,0]);
};