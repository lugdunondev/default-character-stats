Namespace.declare("net.lugdunon.world.defaults.character.modifier");
Namespace.newClass("net.lugdunon.world.defaults.character.modifier.IntoxicatedModifier","net.lugdunon.state.character.modifier.BaseModifier");

// //

net.lugdunon.world.defaults.character.modifier.IntoxicatedModifier.prototype.init=function(initData)
{
	this.callSuper(net.lugdunon.world.defaults.character.modifier.IntoxicatedModifier,"init",[initData]);

	return(this);
};

net.lugdunon.world.defaults.character.modifier.IntoxicatedModifier.prototype.renderTooltip=function()
{
	var content=this.callSuper(net.lugdunon.world.defaults.character.modifier.IntoxicatedModifier,"renderTooltip",[]);
	
	content.html+="<div class='ttLineItem' style='color:#F00'>Effect: You become intoxicated, and can no longer benefit from the consumption of brewed beverages.</div>";
	
	return(content);
};