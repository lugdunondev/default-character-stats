Namespace.declare("net.lugdunon.world.defaults.character.modifier");
Namespace.newClass("net.lugdunon.world.defaults.character.modifier.HealthOverTimeModifier","net.lugdunon.state.character.modifier.BaseModifier");

// //

net.lugdunon.world.defaults.character.modifier.HealthOverTimeModifier.HEALTH_LOSE     =0;
net.lugdunon.world.defaults.character.modifier.HealthOverTimeModifier.HEALTH_GAIN     =1;

net.lugdunon.world.defaults.character.modifier.HealthOverTimeModifier.VALUE_PERCENTAGE=0;
net.lugdunon.world.defaults.character.modifier.HealthOverTimeModifier.VALUE_RAW       =1;

net.lugdunon.world.defaults.character.modifier.HealthOverTimeModifier.prototype.init=function(initData)
{
	this.callSuper(net.lugdunon.world.defaults.character.modifier.HealthOverTimeModifier,"init",[initData]);

	if(initData.dataView)
	{
	    this.mode     =initData.dataView.readUint8  ();
	    this.valueMode=initData.dataView.readUint8  ();
	    this.value    =initData.dataView.readFloat64();
	    this.interval =initData.dataView.readFloat64();
	}
	else
	{
	    this.mode     =initData.props.mode;
	    this.valueMode=initData.props.valueMode;
	    this.value    =initData.props.value;
	    this.interval =initData.props.interval;
	}

	return(this);
};

net.lugdunon.world.defaults.character.modifier.HealthOverTimeModifier.prototype.getValue=function()
{
	var value=0;
	
	if(this.definition == true)
	{
		value=this.value+(this.value*game.player.characterStats.getSchoolBonus(game.player,this.school));
	}
	else
	{
		value=this.value+(this.value*this.valueModifier);
	}

	return(((this.valueMode==net.lugdunon.world.defaults.character.modifier.HealthOverTimeModifier.VALUE_PERCENTAGE)?((value*100).toFixed(2)+"%"):(value)));
};

net.lugdunon.world.defaults.character.modifier.HealthOverTimeModifier.prototype.renderTooltip=function()
{
	var content=this.callSuper(net.lugdunon.world.defaults.character.modifier.HealthOverTimeModifier,"renderTooltip",[]);
	var value  =this.getValue();

	if(this.school == "LIGHT")
	{
		content.html+="<div class='ttLineItem' style='color:"+((this.mode==net.lugdunon.world.defaults.character.modifier.StaminaOverTimeModifier.HEALTH_LOSE)?("#F00"):("#0F0"))+
		"'>Effect: "+((this.mode==net.lugdunon.world.defaults.character.modifier.HealthOverTimeModifier.HEALTH_LOSE)?("Removes"):("Restores"))+
		" "+((this.valueMode==net.lugdunon.world.defaults.character.modifier.HealthOverTimeModifier.VALUE_PERCENTAGE)?(value+" of the target's maximum"):(value))+" health every "+
		(this.interval/1000).toFixed(2)+" seconds for "+
		(this.duration/1000).toFixed(2)+" seconds. This modifier has the opposite effect against the undead.</div>";
	}
	else if(this.school == "SHADOW")
	{
		content.html+="<div class='ttLineItem' style='color:"+((this.mode==net.lugdunon.world.defaults.character.modifier.StaminaOverTimeModifier.HEALTH_LOSE)?("#F00"):("#0F0"))+
		"'>Effect: "+((this.mode==net.lugdunon.world.defaults.character.modifier.HealthOverTimeModifier.HEALTH_LOSE)?("Restores"):("Removes"))+
		" "+((this.valueMode==net.lugdunon.world.defaults.character.modifier.HealthOverTimeModifier.VALUE_PERCENTAGE)?(value+" of the target's maximum"):(value))+" health every "+
		(this.interval/1000).toFixed(2)+" seconds for "+
		(this.duration/1000).toFixed(2)+" seconds. This modifier has the opposite effect against the undead.</div>";
	}
	
	return(content);
};