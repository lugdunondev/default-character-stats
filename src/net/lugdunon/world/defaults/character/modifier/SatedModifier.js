Namespace.declare("net.lugdunon.world.defaults.character.modifier");
Namespace.newClass("net.lugdunon.world.defaults.character.modifier.SatedModifier","net.lugdunon.state.character.modifier.BaseModifier");

// //

net.lugdunon.world.defaults.character.modifier.SatedModifier.prototype.init=function(initData)
{
	this.callSuper(net.lugdunon.world.defaults.character.modifier.SatedModifier,"init",[initData]);

	return(this);
};

net.lugdunon.world.defaults.character.modifier.SatedModifier.prototype.renderTooltip=function()
{
	var content=this.callSuper(net.lugdunon.world.defaults.character.modifier.SatedModifier,"renderTooltip",[]);
	
	content.html+="<div class='ttLineItem' style='color:#F00'>Effect: You become sated, and can no longer benefit from the consumption of food.</div>";
	
	return(content);
};