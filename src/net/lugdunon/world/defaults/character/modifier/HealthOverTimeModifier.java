package net.lugdunon.world.defaults.character.modifier;

import java.io.ByteArrayOutputStream;

import net.lugdunon.io.EnhancedDataOutputStream;
import net.lugdunon.state.character.modifier.BaseModifier;
import net.lugdunon.state.character.stats.ICharacterStats;

import org.json.JSONException;
import org.json.JSONObject;

public class HealthOverTimeModifier extends BaseModifier
{
	public static final int HEALTH_LOSE     =0;
	public static final int HEALTH_GAIN     =1;
	
	public static final int VALUE_PERCENTAGE=0;
	public static final int VALUE_RAW       =1;
	
	private int    mode;
	private int    valueMode;
	private double value;
	private long   interval;
	
	private int    modify;
	
	private long   accumulatedDelta;
	
	public HealthOverTimeModifier(ICharacterStats characterStats, JSONObject o, boolean isRehydrating)
    {
	    super(characterStats, o, isRehydrating);
	    
	    accumulatedDelta=0;
	    
	    try
	    {
		    mode     =getProps().getInt   ("mode"     );
		    valueMode=getProps().getInt   ("valueMode");
		    value    =getProps().getDouble("value"    );
		    interval =getProps().getLong  ("interval" );
	    }
	    catch(Exception e)
	    {
	    	e.printStackTrace();
	    }
    }
	
	public void update (long delta)
	{
		super.update(delta);
		
		this.accumulatedDelta+=delta;

		//HEALTH MODIFY OVER TIME
		if(this.accumulatedDelta >= interval)
		{
			this.accumulatedDelta=0;
			
			if(valueMode == VALUE_PERCENTAGE)
			{
				modify=(int) Math.ceil(value*getCharacterStats().getMaximumHealth());
			}
			else if(valueMode == VALUE_RAW)
			{
				modify=(int) Math.ceil(value);
			}
			
			if(mode == HEALTH_LOSE)
			{
				modify*=-1;
			}
			
			if(
				(("LIGHT" ).equals(getSchool()) &&  getCharacterStats().getCharacter().isUndead()) ||
				(("SHADOW").equals(getSchool()) && !getCharacterStats().getCharacter().isUndead())
			)
			{
				modify*=-1;
			}
			
			int ha=getCharacterStats().getHealingAmount(modify,ICharacterStats.HEALTH_MODIFY_CAUSE_MODIFIER,this);
			       getCharacterStats().modifyHealth    (ha,    ICharacterStats.HEALTH_MODIFY_CAUSE_MODIFIER,this);
		} 
	}
	
	@Override
	public JSONObject toJSONObject() throws JSONException
	{
		JSONObject o=super.toJSONObject();

		o.put("mode",      mode);
		o.put("valueMode",valueMode);
		o.put("value",    value);
		o.put("interval", interval);
	
		return(o);
	}

	@Override
	public byte[] toBytes()
	{
	    EnhancedDataOutputStream out=null;
	    
		try
		{
		    ByteArrayOutputStream baos=new ByteArrayOutputStream();
		    
		    out=new EnhancedDataOutputStream(baos);

		    out.write(super.toBytes());
		    
		    out.writeByte  (mode     );
		    out.writeByte  (valueMode);
		    out.writeDouble(value    );
		    out.writeDouble(interval );
		    
		    return(baos.toByteArray());
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try{out.close();}catch(Exception e){};
		}
		
		return(null);
	}
}