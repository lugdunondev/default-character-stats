package net.lugdunon.world.defaults.character.modifier;

import net.lugdunon.state.character.modifier.BaseModifier;
import net.lugdunon.state.character.stats.ICharacterStats;
import net.lugdunon.state.character.Character;
import net.lugdunon.world.defaults.character.modifier.ex.AlreadyUndeadException;

import org.json.JSONObject;

public class TurnUndeadModifier extends BaseModifier
{
	public TurnUndeadModifier(ICharacterStats characterStats, JSONObject o, boolean isRehydrating) throws AlreadyUndeadException
	{
		super(characterStats, o, isRehydrating);
		
		if(!isRehydrating && characterStats.getCharacter().isUndead())
		{
			throw new AlreadyUndeadException();
		}
		
		getCharacterStats().getCharacter().setUndead(true);
	}
	
	@Override
    public boolean reapply(double valueModifier, Character applier)
    {
		if(getCharacterStats().getCharacter().isUndead())
		{
			return(false);
		}
		
		getCharacterStats().getCharacter().setUndead(true);
		
	    return super.reapply(valueModifier, applier);
    }

	@Override
    public void expire()
    {
	    super.expire();
		
		getCharacterStats().getCharacter().setUndead(false);
    }
}