package net.lugdunon.world.defaults.character.modifier;

import java.io.ByteArrayOutputStream;

import net.lugdunon.io.EnhancedDataOutputStream;
import net.lugdunon.state.character.modifier.BaseModifier;
import net.lugdunon.state.character.stats.ICharacterStats;
import net.lugdunon.world.defaults.character.DefaultCharacterStats;
import net.lugdunon.state.character.Character;

import org.json.JSONException;
import org.json.JSONObject;

public class CharacterStatModifier extends BaseModifier
{
	public static final int TYPE_PERCENTAGE=0;
	public static final int TYPE_RAW       =1;
	
	private String statType;
	private int    valueType;
	private double value;
	
	private DefaultCharacterStats characterStats;
	
	public CharacterStatModifier(ICharacterStats characterStats, JSONObject o, boolean isRehydrating)
	{
		super(characterStats, o, isRehydrating);
		this.characterStats=(DefaultCharacterStats) characterStats;
		
		if(isRehydrating)
		{
			this.characterStats.updateStatModifier(DefaultCharacterStats.STAT_MODIFIER_MODE_ADD,    statType,new Double(value+(value*getValueModifier())));
			this.characterStats.informResourceChangeDelegate(null,new String[]{statType});
		}
	}
	
	@Override
	public boolean reapply(double valueModifier, Character applier)
	{
		this.characterStats.updateStatModifier(DefaultCharacterStats.STAT_MODIFIER_MODE_SUBTRACT,statType,new Double(value+(value*getValueModifier())));
		this.characterStats.informResourceChangeDelegate(null,new String[]{statType});
		
		return(super.reapply(valueModifier, applier));
	}

	@Override
	public void setValueModifierWithApplier(double valueModifier, Character applier)
	{
		super.setValueModifierWithApplier(valueModifier,applier);

		this.characterStats.updateStatModifier(DefaultCharacterStats.STAT_MODIFIER_MODE_ADD,    statType,new Double(value+(value*getValueModifier())));
		this.characterStats.informResourceChangeDelegate(null,new String[]{statType});
	}
	
	////

	@Override
	public void expire()
	{
		super.expire();
		
		this.characterStats.updateStatModifier(DefaultCharacterStats.STAT_MODIFIER_MODE_SUBTRACT,statType,new Double(value+(value*getValueModifier())));
		this.characterStats.informResourceChangeDelegate(null,new String[]{statType});
	}
	
	
	////


	@Override
	public void fromJSONObject(JSONObject o) throws JSONException
	{
		super.fromJSONObject(o);
		
		value    =getProps().getDouble("value"    );
		valueType=getProps().getInt   ("valueType");
		statType =getProps().getString("statType" );
	}

	@Override
	public byte[] toBytes()
	{
	    EnhancedDataOutputStream out=null;
	    
		try
		{
		    ByteArrayOutputStream baos=new ByteArrayOutputStream();
		    
		    out=new EnhancedDataOutputStream(baos);

		    out.write(super.toBytes());
		    
		    out.writeDouble(value    );
		    out.writeByte  (valueType);
		    out.writeUTF   (statType );
		    
		    return(baos.toByteArray());
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try{out.close();}catch(Exception e){};
		}
		
		return(null);
	}
}