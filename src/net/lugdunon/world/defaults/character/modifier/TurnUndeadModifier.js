Namespace.declare("net.lugdunon.world.defaults.character.modifier");
Namespace.newClass("net.lugdunon.world.defaults.character.modifier.TurnUndeadModifier","net.lugdunon.state.character.modifier.BaseModifier");

// //

net.lugdunon.world.defaults.character.modifier.TurnUndeadModifier.prototype.init=function(initData)
{
	this.callSuper(net.lugdunon.world.defaults.character.modifier.TurnUndeadModifier,"init",[initData]);

	return(this);
};

net.lugdunon.world.defaults.character.modifier.TurnUndeadModifier.prototype.renderTooltip=function()
{
	var content=this.callSuper(net.lugdunon.world.defaults.character.modifier.TurnUndeadModifier,"renderTooltip",[]);
	
	content.html+="<div class='ttLineItem' style='color:#F00'>Effect: You are counted among the undead.</div>";
	
	return(content);
};