package net.lugdunon.world.defaults.character.modifier;

import net.lugdunon.state.character.modifier.BaseModifier;
import net.lugdunon.state.character.stats.ICharacterStats;

import org.json.JSONObject;

public class IntoxicatedModifier extends BaseModifier
{
	public IntoxicatedModifier(ICharacterStats characterStats, JSONObject o, boolean isRehydrating)
	{
		super(characterStats, o, isRehydrating);
	}
}