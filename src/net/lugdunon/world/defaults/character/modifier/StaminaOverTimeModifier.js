Namespace.declare("net.lugdunon.world.defaults.character.modifier");
Namespace.newClass("net.lugdunon.world.defaults.character.modifier.StaminaOverTimeModifier","net.lugdunon.state.character.modifier.BaseModifier");

// //

net.lugdunon.world.defaults.character.modifier.StaminaOverTimeModifier.HEALTH_LOSE     =0;
net.lugdunon.world.defaults.character.modifier.StaminaOverTimeModifier.HEALTH_GAIN     =1;

net.lugdunon.world.defaults.character.modifier.StaminaOverTimeModifier.VALUE_PERCENTAGE=0;
net.lugdunon.world.defaults.character.modifier.StaminaOverTimeModifier.VALUE_RAW       =1;

net.lugdunon.world.defaults.character.modifier.StaminaOverTimeModifier.prototype.init=function(initData)
{
	this.callSuper(net.lugdunon.world.defaults.character.modifier.StaminaOverTimeModifier,"init",[initData]);
	
	if(initData.dataView)
	{
	    this.mode     =initData.dataView.readUint8  ();
	    this.valueMode=initData.dataView.readUint8  ();
	    this.value    =initData.dataView.readFloat64();
	    this.interval =initData.dataView.readFloat64();
	}
	else
	{
	    this.mode     =initData.props.mode;
	    this.valueMode=initData.props.valueMode;
	    this.value    =initData.props.value;
	    this.interval =initData.props.interval;
	}
	
	return(this);
};

net.lugdunon.world.defaults.character.modifier.StaminaOverTimeModifier.prototype.getValue=function()
{
	var value=0;
	
	if(this.definition == true)
	{
		value=this.value+(this.value*game.player.characterStats.getSchoolBonus(game.player,this.school));
	}
	else
	{
		value=this.value+(this.value*this.valueModifier);
	}
	
	return(((this.valueMode==net.lugdunon.world.defaults.character.modifier.HealthOverTimeModifier.VALUE_PERCENTAGE)?((value*100).toFixed(2)+"%"):(value)));
};

net.lugdunon.world.defaults.character.modifier.StaminaOverTimeModifier.prototype.renderTooltip=function()
{
	var content=this.callSuper(net.lugdunon.world.defaults.character.modifier.StaminaOverTimeModifier,"renderTooltip",[]);
	var value  =this.value+(this.value*this.valueModifier);
	
	content.html+="<div class='ttLineItem' style='color:"+((this.mode==net.lugdunon.world.defaults.character.modifier.StaminaOverTimeModifier.HEALTH_LOSE)?("#F00"):("#0F0"))+
	"'>Effect: "+((this.mode==net.lugdunon.world.defaults.character.modifier.StaminaOverTimeModifier.HEALTH_LOSE)?("Removes"):("Restores"))+
	" "+((net.lugdunon.world.defaults.character.modifier.StaminaOverTimeModifier.VALUE_PERCENTAGE)?(value+" of the target's maximum"):(value))+" stamina every "+
	(this.interval/1000).toFixed(2)+" seconds for "+
	(this.duration/1000).toFixed(2)+" seconds.</div>";
	
	return(content);
};