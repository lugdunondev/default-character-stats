package net.lugdunon.world.defaults.character.modifier;

import net.lugdunon.state.character.modifier.BaseModifier;
import net.lugdunon.state.character.stats.ICharacterStats;

import org.json.JSONObject;

public class SatedModifier extends BaseModifier
{
	public SatedModifier(ICharacterStats characterStats, JSONObject o, boolean isRehydrating)
	{
		super(characterStats, o, isRehydrating);
	}
}