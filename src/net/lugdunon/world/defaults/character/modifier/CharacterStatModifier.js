Namespace.declare("net.lugdunon.world.defaults.character.modifier");
Namespace.newClass("net.lugdunon.world.defaults.character.modifier.CharacterStatModifier","net.lugdunon.state.character.modifier.BaseModifier");

// //

net.lugdunon.world.defaults.character.modifier.CharacterStatModifier.STATS_TO_DISPLAY={
	healedBonus      :"that you are healed",
	healingBonus     :"that you heal for",
	foodBonus        :"that food does for you",
	feedBonus        :"that your food does for others",
	damageBonus      :"of damage that you do",
	damageBonus_HAND :"of damage that you do barehanded",
	damageBonus_AXE  :"of damage that you do with axes",
	damageBonus_SWORD:"of damage that you do with swords",
	damageBonus_BOW  :"of damage that you do with a bow",
	healthIncrease   :"of health you have",
	staminaIncrease  :"of stamina you have"
};

net.lugdunon.world.defaults.character.modifier.CharacterStatModifier.TYPE_PERCENTAGE=0;
net.lugdunon.world.defaults.character.modifier.CharacterStatModifier.TYPE_RAW       =1;

net.lugdunon.world.defaults.character.modifier.CharacterStatModifier.prototype.init=function(initData)
{
	this.callSuper(net.lugdunon.world.defaults.character.modifier.CharacterStatModifier,"init",[initData]);

	if(initData.dataView)
	{
	    this.value    =initData.dataView.readFloat64();
	    this.valueType=initData.dataView.readUint8  ();
	    this.statType =initData.dataView.readString ();
	}
	else
	{
	    this.value    =this.props.value;
	    this.valueType=this.props.valueType;
	    this.statType =this.props.statType;
	}
	
	return(this);
};

net.lugdunon.world.defaults.character.modifier.CharacterStatModifier.prototype.getValue=function()
{
	var value=0;

	if(this.definition == true)
	{
		value=this.value+(this.value*game.player.characterStats.getSchoolBonus(game.player,this.school));

	}
	else
	{

		value=this.value+(this.value*this.valueModifier);
	}

	return(
		this.valueType == net.lugdunon.world.defaults.character.modifier.CharacterStatModifier.TYPE_PERCENTAGE?((value*100)+"%"):
		this.valueType == net.lugdunon.world.defaults.character.modifier.CharacterStatModifier.TYPE_RAW       ?( value    ):"???"
	);
};

net.lugdunon.world.defaults.character.modifier.CharacterStatModifier.prototype.getNumericValue=function()
{
	var value=0;

	if(this.definition == true)
	{
		value=this.value+(this.value*game.player.characterStats.getSchoolBonus(game.player,this.school));

	}
	else
	{

		value=this.value+(this.value*this.valueModifier);
	}

	return(
		this.valueType == net.lugdunon.world.defaults.character.modifier.CharacterStatModifier.TYPE_PERCENTAGE?((value*100)):
		this.valueType == net.lugdunon.world.defaults.character.modifier.CharacterStatModifier.TYPE_RAW       ?( value    ):0
	);
};

net.lugdunon.world.defaults.character.modifier.CharacterStatModifier.prototype.renderTooltip=function()
{
	var content=this.callSuper(net.lugdunon.world.defaults.character.modifier.CharacterStatModifier,"renderTooltip",[]);
	
	content.html+="<div class='ttLineItem' style='color:"+((this.getNumericValue() > 0)?"#0F0":"#F00")+"'>Effect: "+
	((this.getNumericValue() > 0)?"Increases":"Decreases")+
	" the amount "+
	net.lugdunon.world.defaults.character.modifier.CharacterStatModifier.STATS_TO_DISPLAY[this.statType]+
	" by "+this.getValue()+
	"</div>";
	
	return(content);
};