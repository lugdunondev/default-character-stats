package net.lugdunon.world.defaults.character;

import net.lugdunon.server.worldgen.ex.WorldIdNotSetException;
import net.lugdunon.state.SubsystemBase;
import net.lugdunon.state.character.stats.ICharacterStatsSystem;
import net.lugdunon.util.FileUtils;

import org.json.JSONException;
import org.json.JSONObject;

public class DefaultCharacterStatsSystem extends SubsystemBase implements ICharacterStatsSystem
{
	private static final String[] DEFAULT_CONFIGS=new String[]{"stats"};
	
	private String     characterStats;
	private JSONObject characterStatsDefinition;
	
	public DefaultCharacterStatsSystem() throws WorldIdNotSetException
	{
		JSONObject cfg=getConfigObject("stats");

		characterStatsDefinition=cfg;
		
		try
        {
	        characterStats=cfg.getString("characterStats");
        }
        catch (JSONException e)
        {
        	characterStats="net.lugdunon.world.defaults.character.DefaultCharacterStats";
        }
	}
	
	////
	
	@Override
	public String getCharacterStatsImpl()
	{
		return(characterStats);
	}
	
	@Override
	public JSONObject getCharacterStatsDefinition()
	{
		return(characterStatsDefinition);
	}
	
	////
	
	@Override
	public String getNamespace()
	{
		return("net.lugdunon.world.defaults.character");
	}

	@Override
    protected String[] getDefaultConfigs()
    {
	    return(DEFAULT_CONFIGS);
    }

	@Override
    public JSONObject dumpState() throws WorldIdNotSetException
    {
		try
        {
	        return(new JSONObject(characterStatsDefinition.toString()));
        }
        catch (JSONException e)
        {
	        e.printStackTrace();
        }
		
		return(null);
    }

	@Override
    public void saveState() throws WorldIdNotSetException
    {
		saveConfigObject("stats",characterStatsDefinition);
    }

	@Override
    public String getMD5Hash()
    {
	    return(FileUtils.genMD5Hash(getCharacterStatsDefinition()));
    }
}