package net.lugdunon.world.defaults.character;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;

import net.lugdunon.command.Command;
import net.lugdunon.command.CommandProperties;
import net.lugdunon.command.CommandRequest;
import net.lugdunon.command.core.IServerInvokedCommand;
import net.lugdunon.command.ex.ConsoleFiredCommandNotSupportedException;
import net.lugdunon.state.State;
import net.lugdunon.state.character.Character;
import net.lugdunon.state.character.NonPlayerCharacter;
import net.lugdunon.state.character.PlayerCharacter;

public class DefaultCharacterStatModifierChangeCommand extends Command implements IServerInvokedCommand
{
	protected Response  iRes;
	
	////
	
	@Override
	public String getCommandId()
	{
	    return("CORE.COMMAND.CHARACTER.STAT.MODIFIER.CHANGE");
	}

	@Override
	public String getName()
	{
		return("Character stat modifier change");
	}

	@Override
	public String getDescription()
	{
		return("Handles a character's stat modifier change notification.");
	}

	@Override
	public boolean hasClientSide()
	{
		return(true);
	}

	@Override
	public int getCommandLength()
	{
	    return(iRes.length());
	}

	@SuppressWarnings("unchecked")
    @Override
	public void handle(CommandProperties props) throws IOException, ConsoleFiredCommandNotSupportedException
	{
		Character             character   =props.getCharacter("character"   );
		String                statModifier=props.getString   ("statModifier");
		
		Object                statModifierValue=((DefaultCharacterStats) character.getCharacterStats()).statModifiers.get(statModifier);
		List<PlayerCharacter> chars            =State.instance().listActiveCharactersInInstance(character.getInstanceId());
		Response              oRes;

		iRes=initializeInternalResponse();

		iRes.out.writeUTF(statModifier);
		
		if(character instanceof NonPlayerCharacter)
		{
			iRes.out.write      (0);
			iRes.out.writeDouble(((NonPlayerCharacter) character).getNpcId());
		}
		else if(character instanceof PlayerCharacter)
		{
			iRes.out.write      (1);
			iRes.out.writeUTF   (character.getName());
		}
		
		if(statModifierValue == null)
		{
			iRes.out.writeBoolean(false);
		}
		else if(statModifierValue instanceof Double)
    	{
			iRes.out.writeBoolean(true);
    		iRes.out.writeDouble (((Double) statModifierValue));
    	}
    	else if(statModifierValue instanceof Long)
    	{
			iRes.out.writeBoolean(true);
    		iRes.out.writeDouble (((Long  ) statModifierValue));
    	}
    	else
    	{
    		String[] ss=(String[]) ((ConcurrentLinkedQueue<String>) statModifierValue).toArray();

			iRes.out.writeBoolean(true);
    		iRes.out.writeShort  (ss.length);
			
			for(String s:ss)
			{
				iRes.out.writeUTF(s);
			}
    	}

		oRes=initializeResponse();
		oRes.out.write(iRes.bytes());

		if(chars != null)
		{
			for(PlayerCharacter c:chars)
			{
				c.getAccount().getConnection().sendMessage(oRes.bytes(),0,oRes.bytes().length);
			}
		}
	}

	@Override
	public void handle(CommandRequest request) throws IOException, ConsoleFiredCommandNotSupportedException
	{
		//DO NOTHING
	}
}