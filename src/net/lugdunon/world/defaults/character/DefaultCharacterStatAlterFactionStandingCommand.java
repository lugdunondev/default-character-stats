package net.lugdunon.world.defaults.character;

import java.io.IOException;
import java.util.List;

import net.lugdunon.command.Command;
import net.lugdunon.command.CommandProperties;
import net.lugdunon.command.CommandRequest;
import net.lugdunon.command.core.IServerInvokedCommand;
import net.lugdunon.command.ex.ConsoleFiredCommandNotSupportedException;
import net.lugdunon.state.State;
import net.lugdunon.state.character.Character;
import net.lugdunon.state.character.NonPlayerCharacter;
import net.lugdunon.state.character.PlayerCharacter;

import org.json.JSONObject;

public class DefaultCharacterStatAlterFactionStandingCommand extends Command implements IServerInvokedCommand
{
	protected Response  iRes;
	
	////
	
	@Override
	public String getCommandId()
	{
	    return("CORE.COMMAND.CHARACTER.STAT.ALTER.FACTION.STANDING");
	}

	@Override
	public String getName()
	{
		return("Character stat alter faction standing");
	}

	@Override
	public String getDescription()
	{
		return("Handles a character's faction standing alteration notification.");
	}

	@Override
	public boolean hasClientSide()
	{
		return(true);
	}

	@Override
	public int getCommandLength()
	{
	    return(iRes.length());
	}

    @Override
	public void handle(CommandProperties props) throws IOException, ConsoleFiredCommandNotSupportedException
	{
		Character             character        =props.getCharacter("character"        );
		long                  factionId        =props.getLong     ("factionId"        );
		int                   factionAlteration=props.getInt      ("factionAlteration");
		int                   factionStanding  =props.getInt      ("factionStanding"  );
		
		List<PlayerCharacter> chars            =State.instance().listActiveCharactersInInstance(character.getInstanceId());
		Response              oRes;

		iRes=initializeInternalResponse();
		
		if(character instanceof NonPlayerCharacter)
		{
			iRes.out.write      (0);
			iRes.out.writeDouble(((NonPlayerCharacter) character).getNpcId());
		}
		else if(character instanceof PlayerCharacter)
		{
			iRes.out.write      (1);
			iRes.out.writeUTF   (character.getName());
			
			try
			{
				JSONObject fsa=new JSONObject();
	
				fsa.put("factionId",  String.valueOf(factionId        ));
				fsa.put("alteration",                factionAlteration );
				fsa.put("standing",                  factionStanding   );
				
				((PlayerCharacter) character).getMetricsManager().handleMetricUpdate(
					"CORE.METRIC.FACTION.STANDING.ALTERED",
					fsa
				);
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}

		iRes.out.writeDouble(factionId        );
		iRes.out.writeInt   (factionAlteration);
		iRes.out.writeInt   (factionStanding  );

		oRes=initializeResponse();
		oRes.out.write(iRes.bytes());

		if(chars != null)
		{
			for(PlayerCharacter c:chars)
			{
				c.getAccount().getConnection().sendMessage(oRes.bytes(),0,oRes.bytes().length);
			}
		}
	}

	@Override
	public void handle(CommandRequest request) throws IOException, ConsoleFiredCommandNotSupportedException
	{
		//DO NOTHING
	}
}